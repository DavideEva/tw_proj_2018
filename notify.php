<?php
  include 'static.php';
  session_start();
  if(isset($_SESSION['nickname']) && isset($_GET['funct'])){
    if ($_GET['funct'] == "showNotify"){
      showNotify($_SESSION['nickname']);
    } else if ($_GET['funct'] == "updateNotify" && isset($_GET['ordineid']) && isset($_GET['email']) && isset($_GET['ristoID'])){
      updateNotify($_GET['ordineid'],$_GET['email'],$_GET['ristoID']);
    } else if($_GET['funct'] == "getfattemail"){
      getFattoriniEmail($_SESSION['nickname']);
    } else if ($_GET['funct'] == "ordineConsegnato" && isset($_GET['ordineid']) && isset($_GET['ristoID'])){
      ordineConsegnato($_SESSION['nickname'],$_GET['ordineid'],$_GET['ristoID']);
    } else if ($_GET['funct'] == "ordineEvaso" && isset($_GET['ordineid']) && isset($_GET['ristoID'])){
      ordineEvaso($_SESSION['nickname'],$_GET['ordineid'],$_GET['ristoID']);
    }
  }

function updateNotify(int $ordid, string $emailfatt,int $ristoID){
  $string1= ", `fattorinoID`= '$emailfatt'";
  if ($emailfatt == 'null'){
    $string1 = '';
  }
  $sqlinsert = "UPDATE `ordine`
                SET `tipologiaOrdine`= 'ACCETTATO' $string1
                WHERE `ordine`.id = $ordid
                AND `ordine`.ristoID = $ristoID
                AND `ordine`.tipologiaOrdine = 'INVIATO'"; //id ordine
  if (MyClass::updateIt($sqlinsert,false) === TRUE){
    echo (1);
    return 1;
  }
}

function ordineEvaso(string $nick, int $ordid, int $restID){
  $sqlinsert = "UPDATE `ordine`
                SET `ordine`.tipologiaOrdine = 'EVASO'
                WHERE `ordine`.ristoID = $restID
                AND `ordine`.id = $ordid
                AND `ordine`.tipologiaOrdine = 'ACCETTATO'"; //id ordine
  if (MyClass::updateIt($sqlinsert,false) === TRUE){
    echo (1);
    return 1;
  }
}

function ordineConsegnato(string $nick, int $ordid, int $restID){
  $email = MyClass::getEmailFromNick($nick);
  $sqlinsert = "UPDATE `ordine`
                SET `ordine`.tipologiaOrdine = 'CONSEGNATO'
                WHERE `ordine`.compratoreID = '$email'
                AND `ordine`.ristoID = $restID
                AND `ordine`.id = $ordid
                AND `ordine`.tipologiaOrdine = 'EVASO'"; //id ordine
    MyClass::updateIt($sqlinsert,false);

}

  function showNotify(string $nick){
    $email = MyClass::getEmailFromNick($nick);
    $sql = "SELECT `utente`.tipologia FROM `utente`
            WHERE `utente`.nickname='$nick'";
    $result = MyClass::queryIt($sql,false);
    if ($result!==null && $result->num_rows == 1) {
      $strtipo = $result->fetch_assoc()['tipologia'];
      $sql2="";
      switch($strtipo){
      case 'ristoratore':

          $sql2 = "SELECT `ordine`.id, `ristorante`.id AS ristoID, `ristorante`.nome, `piatto`.nomePiatto, `ordine`.quantita, `ordine`.orario FROM `ordine`
                   JOIN `ristorante` ON `ordine`.ristoID = `ristorante`.id
                   JOIN `utente` ON `ristorante`.gestoreID = `utente`.email
                   JOIN `piatto` ON `ordine`.piattoID = `piatto`.piattoID
                   WHERE `utente`.email = '$email'
                   AND `ordine`.tipologiaOrdine = 'INVIATO'
                   ORDER BY `ordine`.id";
          $resultinner = MyClass::queryIt($sql2,false);

          if ($resultinner !== null && $resultinner->num_rows>= 1){
            ?>
            <h2>nuovi ordini rilevati</h2>
            <?php
            $lastID = 0;
            while($row = $resultinner->fetch_assoc()){
              if ($row['id'] > $lastID){
                if ($lastID != 0){
                  ?></span></div><?php
                }
                $lastID = $row['id'];
                ?><div id="div_ordine<?=$row['id']?>"><span>il ristorante <?=$row['nome']?> ha un ordine per il <?=$row['orario']?></span><br/>
                  <input class="form-control" id="in_ordine<?=$row['id']?>" type="email" name="email" placeholder="email fattorino"/>
                  <button class="btn" onclick="accettato(<?=$row['id']?>,<?=$row['ristoID']?>)">assegna al fattorino</button>
                  <button class="btn" onclick="accettatoAutonomo(<?=$row['id']?>,<?=$row['ristoID']?>,null)">consegna autonoma</button>
                <?php

              }
              ?>
              <div><?=$row['quantita']?>x <?=$row['nomePiatto']?></div>
              <?php
            }
            ?></div>
            <?php
          }
          break;

      case 'fattorino':
          $sql2 = "SELECT DISTINCT `ordine`.id, `ordine`.orario, `ordine`.luogo FROM `ordine`
                   JOIN `utente` ON `ordine`.fattorinoID = `utente`.email
                   WHERE `ordine`.fattorinoID='$email'
                   AND `ordine`.tipologiaOrdine = 'ACCETTATO'
                   ORDER BY `ordine`.orario, `ordine`.id";

          $resultinner = MyClass::queryIt($sql2,false);
          if ($resultinner !== null && $resultinner->num_rows>= 1){
            $vowel = array('a','e','i','o','u','A','E','I','O','U');
            ?>
            <h2>impegni rilevati</h2>
            <?php
            while($row = $resultinner->fetch_assoc()){
              //if ($row['orario'] != '' && $row['luogo'] != ''){
                ?>
                <div>consegna il <?=$row['orario']?> per <?= in_array($row['luogo']{0}, $vowel) ? "l'".$row['luogo'] : "il ".$row['luogo']?></div>
                <?php
              //}
            }
          }
          break;
      case 'Admin':
          //code to be executed if n=label3;
          break;
      case 'compratore':
          $sql2 = "SELECT DISTINCT `ordine`.id, `ordine`.orario, `piatto`.nomePiatto, `ordine`.quantita,
                                   `ordine`.tipologiaOrdine, `ristorante`.nome, `ristorante`.id as ristoID FROM `ordine`
                   JOIN `utente` ON `ordine`.compratoreID = `utente`.email
                   JOIN `piatto` ON `ordine`.piattoID = `piatto`.piattoID
                   JOIN `ristorante` ON `ristorante`.id = `ordine`.ristoID
                   WHERE `ordine`.compratoreID='$email'
                   AND (`ordine`.tipologiaOrdine = 'EVASO'
                     OR `ordine`.tipologiaOrdine = 'ACCETTATO')
                   ORDER BY `ordine`.id, `ordine`.ristoID";
           $resultinner = MyClass::queryIt($sql2,false);

           if ($resultinner !== null && $resultinner->num_rows>= 1){
             ?>
             <h2>Ordini in Arrivo</h2>
             <?php
             $lastID = 0;
             $lastrist = 0;
             while($row = $resultinner->fetch_assoc()){
               if ($row['id'] > $lastID){
                 if ($lastID != 0){
                   ?></span></div><?php
                 }
                 $lastrist = 0;
                 $lastID = $row['id'];
                 ?><div id="div_ordine<?=$row['id']?>"><span>hai un ordine per il <?=$row['orario']?></span>
                 <?php

               }
               if ($lastrist != $row['ristoID']){
                 if ($lastrist != 0){
                   ?></span><?php
                 }
                 $inviato = $row['tipologiaOrdine'] == 'EVASO' ? "in consegna" : "da evadere";
                 $lastrist =  $row['ristoID'];
                 ?><span id=span_ordine<?=$row['id']?><?=$row['ristoID']?>>dal ristorante <?=$row['nome']?> <?=$inviato?>
                 <?php if($row['tipologiaOrdine'] == 'EVASO'){?>
                   <button onclick="ricevuto(<?=$row['id']?>,<?=$row['ristoID']?>)">ordine ricevuto</button><?php
                 }
               }
               ?>
               <div><?=$row['quantita']?>x <?=$row['nomePiatto']?>
               </div>
               <?php
             }
             ?></div>
             <?php
           }
          break;
      default:
          echo "error";
          return;
      }
      //echo $resultinner->num_rows;
    } else {
      ?><span> non sono state rilevate notifiche :( </span> <?php
    }
  }

  function getFattoriniEmail($nick){
    if (MyClass::getTipologiaFromNick($nick) == 'ristoratore'){
      $sql1 = "SELECT `ristorante`.id FROM `utente`
              JOIN `ristorante` ON `ristorante`.gestoreID = `utente`.email
              WHERE `utente`.nickname ='$nick'";
              $res1=MyClass::queryIt($sql1,false);
      if ($res1 !== null && $res1->num_rows >= 1){
        $array = array();
        while ($r = $res1->fetch_assoc()){
          $ristID = $r['id'];
          $sql = "SELECT `utente`.email FROM `utente`
                  JOIN `ristorante` ON `ristorante`.id = `utente`.ristoID
                  WHERE `ristorante`.id ='$ristID'";
          if ($email=MyClass::queryIt($sql,false)->fetch_assoc()){
            array_push($array,$email['email']);
          }
        }
        echo json_encode($array);
      }
    }
  }

  ?>
