function validateEmail(email) {
    var regex = /^(?:[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/;
    return regex.test(email);
}

function registrazione(tipo) {
    event.preventDefault();
    errors = "";

    var nome = $("input#inputNome").val();
    var cognome = $("input#inputCognome").val();
    var email = $("input#inputEmail").val();
    var data_di_nascita = $("input#inputDataDiNascita").val();
    var nickname = $("input#inputNickname").val();
    var password = $("input#inputPassword").val();
    var password2 = $("input#inputPassword2").val();
    var tipologia = tipo;

    if (nome == null || nome.length < 2) {
        errors += "Nome è obbligatorio e deve essere almeno 2 caratteri; ";
    }

    if (cognome == null || cognome.length < 2) {
        errors += "Cognome è obbligatorio e deve essere almeno 2 caratteri; ";
    }

    if (email == null || !validateEmail(email)) {
        errors += "Email è obbligatoria e deve essere valida; ";
    }

    if (errors.length > 0) {
        alert("Data Loaded: " + errors);
    } else {
        var posting = $.post("loginCheck.php", {
            make: "registrazione",
            name: nome,
            surname: cognome,
            mail: email,
            date: data_di_nascita,
            nickname: nickname,
            password: password,
            password2: password2,
            tipologia: tipologia
        })
        posting.done(function (msg) {
            $("#result").html(msg);
            if (msg === 'ok') {
                $(location).attr('href', 'home.html');
            }
            $("#result").show();
        });
    }

};
