<?php
class MyClass
{

        // metodi statici
  public static function findCart(string $nick)
  {
    $sql = "SELECT `ordine`.piattoID, `ordine`.ristoID, `ordine`.quantita, `ordine`.id FROM `ordine`
                  JOIN `utente` ON `utente`.email = `ordine`.compratoreID
                  WHERE `ordine`.compratoreID = `utente`.email
                  AND `utente`.nickname='$nick'
                  AND `ordine`.tipologiaOrdine = 'CARRELLO'
                  ORDER BY `ordine`.ristoID";
    return MyClass::queryIt($sql, false);
  }

  public static function createButton(String $value)
  {
    ?><input type="button" onclick="alert('Hello World!')" value=<?php echo "$value" ?>>
                <?php

              }

              public static function createDoubleStringButton(String $string1, String $string2, float $costo)
              {
                $idstring = str_replace(" ", "_", $string1);
                ?>
          <button class="btn" id="<?= $idstring ?>">
            <p><?= $string1 ?> <?= $costo . '€' ?></p>
            <p><?= $string2 ?></p>
          </button><br/>
        <?php

      }

      public static function createCartButton(String $string1, String $string2, float $costo, String $ordineID, String $emailogged, String $piattoID, String $ristoID, int $quantità = 1)
      {
        $idstring = str_replace(" ", "_", $string1);
        ?>
        <div class="dropdown dropright">
          <button class="btn dropdown-toggle" data-toggle="dropdown" id="<?= $idstring ?>">
            <p><?= $string1 ?> <?= $costo . '€' ?></p>
            <p><?= $string2 ?></p>
          </button>
          <div class="dropdown-menu">
            <div class="dropdown-header">Rimuovi alcuni</div>
            <input class="dropdown-item" type="number" min="1" max="<?= $quantità ?>" class="form-control" id="n<?= $string1 ?>">
            <a class="dropdown-item" onclick="rimuoviNelem('<?= $ordineID ?>', '<?= $emailogged ?>', '<?= $piattoID ?>', '<?= $ristoID ?>', 'n<?= $string1 ?>');">Rimuovi </a>
            <div class="dropdown-divider"></div>
            <div class="dropdown-header">Rimuovi tutto</div>
            <a class="dropdown-item" onclick="rimuovi('<?= $ordineID ?>', '<?= $emailogged ?>', '<?= $piattoID ?>', '<?= $ristoID ?>');">Rimuovi tutto</a>
          </div>
        </div>
          <br/>
        <?php

      }
        /**ingredienti dentro ristorante.html */
        public static function createTripleStringButton(String $string1, String $string2, string $allergeni, float $costo, int $idrist,int $idPiatto) {
                  $idstring = str_replace(" ", "_", $string1);
                ?>
                <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal_<?=$idstring?>" ><p><?=$string1?> <?=$costo.'€'?></p><p><?=$string2?></p></button>
                <!-- Modal -->
                <div id="myModal_<?= $idstring ?>" class="modal fade" role="dialog">
                  <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                      <div class="modal-header">
                        <h4 class="modal-title"><?= $string1 ?></h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                      </div>
                      <div class="modal-body text-center" id="dialog_<?= $idstring ?>">
                        <p>codice allergen<?= strlen($allergeni) == 1 ? 'e: ' . $allergeni : 'i: ' . $allergeni ?></p>
                        <input type="number" id="quantita" name="quantità" oninput="calcolaPrezzo('<?= $idstring ?>', '<?= $costo ?>')" min="1" max="100" step="1" placeholder="quanti ne vuoi?" value=1><br/>
                        <div>costo unitario: <?= $costo ?>€</div> <span id="prezzo_<?= $idstring ?>">costo totale: <?= $costo ?></span><span>€</span><br/>
                        </div>
                        <div class="modal-footer">
                        <button type="submit" class="btn btn-default float-left" data-dismiss="modal" onclick="caricaCarrello('<?= $idstring ?>', '<?= $costo ?>', '<?= $idrist ?>', '<?= $idPiatto ?>')">Aggiungi</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Annulla</button>
                      </div>
                    </div>

                  </div>
                </div>
                <?php

              }

              public static function createDoubleStringAndImageButton(String $string1, String $string2, String $src, String $alt)
              {
                $idstring = str_replace(" ", "_", $string1);
                ?><button id="<?= $idstring ?>">
                  <p><?php echo $string1 ?></p>
                  <p><?php echo $string2 ?></p>
                  <?php MyClass::loadImg($src, $alt); ?>
                </button>
                <br/>
                <?php

              }

              public static function createRestButton(String $string1, String $string2, String $src, String $alt, String $link)
              {
                ?>
              <div class="container media bg-gray rounded">
                <div class="media-body">
                  <h4 class="media-heading"><?php echo $string1 ?></h4>
                  <p><?php echo $string2 ?></p>
                </div>
                <div class="media-right align-self-start">
                  <img src='<?php echo $src ?>' class="ml-3 mt-3" style="width:60px; min-width:50px;" alt='<?php echo $alt ?>'>
                </div>
                <div class="media-right align-self-end">
                  <button type="button" class="btn" onclick='myload("<?php echo $link ?>")'>
                    Ordina da qui
                  </button>
                </div>
              </div>
              <br/>
            <?php

          }

          public static function checkLogin(String $user, String $pass)
          {
            $conn = MyClass::login();
            $sql = "SELECT nickname FROM utente WHERE nickname='$user' AND password=PASSWORD('$pass')";
            $result = $conn->query($sql);
            if (empty($result)) {
              return false;
            }
            if ($result->num_rows == 1) {
              $row = $result->fetch_assoc();
            } else {
              return false;
            }
            return true;
          }

          public static function fetch_allToString(array $array, bool $debug)
          {
            $substring = '';
            if ($debug) var_dump($array);
            foreach ($array as $value) {
              $substring .= $value[0] . ", ";
            }
            $substring = substr($substring, 0, -2);
            if ($debug) var_dump($substring);
            return $substring;
          }

          public static function getNickFromEmail(String $email)
          {
            $sql = "SELECT `utente`.nickname FROM `utente`
                  WHERE `utente`.email='$email'";
            if ($nickname = MyClass::queryIt($sql, false)->fetch_assoc()) {
              return $nickname['nickname'];
            } else {
              return 'not found';
            }
          }

          public static function getFotoFromEmail(String $email)
          {
            $sql = "SELECT `utente`.foto FROM `utente`
                  WHERE `utente`.email='$email'";
            if ($foto = MyClass::queryIt($sql, false)->fetch_assoc()) {
              return $foto['foto'];
            } else {
              return 'empty';
            }
          }

          public static function getEmailFromNick(String $nick)
          {
            $sql = "SELECT `utente`.email FROM `utente`
                  WHERE `utente`.nickname='$nick'";
            if ($email = MyClass::queryIt($sql, false)->fetch_assoc()) {
              return $email['email'];
            } else {
              return 'not found';
            }
          }

          public static function getTipologiaFromNick(String $nick)
          {
            $sql = "SELECT `utente`.tipologia FROM `utente`
                  WHERE `utente`.nickname='$nick'";
            if ($tipo = MyClass::queryIt($sql, false)->fetch_assoc()) {
              return $tipo['tipologia'];
            } else {
              return 'not found';
            }
          }

          public static function queryIt(String $sqlstring, bool $debug)
          {
            if ($sqlstring != "") {
              $conn = MyClass::login();
              $resultsql = $conn->query($sqlstring);
              if ($debug) var_dump($sqlstring);
              if ($resultsql->num_rows >= 1) {
                if ($debug) var_dump($resultsql);
                return $resultsql;
              } else {
                if ($debug) echo "0 result";
                return $resultsql;
              }
            }
            return null;
          }

          public static function updateIt(String $sqlstring, bool $debug)
          {
            $conn = MyClass::login();
            $resultsql = $conn->query($sqlstring);
            if ($debug) var_dump($sqlstring);
            if ($resultsql === true) {
              if ($debug) echo "update OK";
              return $resultsql;
            } else {
              if ($debug) echo "update failed " . $conn->error;
              return $resultsql;
            }
          }

          public static function login()
          {
            $servername = "localhost";
            $username = "root";
            $password = "";
            $dbname = "spaceeat";

          // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
          // Check connection
            if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
            }
            return $conn;
          }

          public static function loadImg(String $name, String $alt = "")
          {
            $file = "file-Immagini/" . $riga_m['file'];//ricavo da db il nome dell'inmmagine e gli do il percorso
            list($width, $height, $type, $attr) = getimagesize($file);//lego i dati
            if (isset($width)) {//verifico la lettura (può capitare che non sia possibile)
              if ($width >= $height) {
                $max_w = "30%";//fisso la larghezza
                $max_h = round($height / $width * 30, 1) . "%";//adatto il % dell'altezza per non deformarw
              } else {
                $max_h = "30%";//fisso l'altezza
                $max_h = round($width / $height * 30, 1) . "%";//adatto il % di width
              }
              $stile = "width: $max_w; height: $max_h;";//preparo lo style
            } else {//non leggo metto solo la width
              $stile = "width: 30%;";
            }
            echo "<img src=\"$name\" style=\"$stile\" title=\"$alt\" alt=\"$alt\" class='img-responsive'>";
          }

          public static function getAllergeniFromIngredienti(String $ingredienti)
          {

          }

          public static function getIngredientiFromFarcitura(String $farcitura)
          {

          }

          public static function getAllergeniFromBase(String $base)
          {

          }

          public static function getAllergeniFromPiatto(int $piattoID)
          {
            $sql2 = "SELECT DISTINCT `allergene`.`nome`  FROM  `piatto`
                  JOIN  `farcitura` ON  `piatto`.piattoID = `farcitura`.piattoID
                  JOIN  `piatto_base` ON `piatto`.piattoID = `piatto_base`.piattoID
                  JOIN  `base_ingrediente` ON `base_ingrediente`.`baseID` = `piatto_base`.baseID
                  JOIN  `ingrediente_allergene` ON (`ingrediente_allergene`.nome = `farcitura`.nome OR
                     																`ingrediente_allergene`.nome = `base_ingrediente`.nome)
                  JOIN  `allergene` ON `allergene`.`allergeneID` = `ingrediente_allergene`.`allergeneID`
                  WHERE `piatto`.piattoID = $piattoID";
            $internquery2 = MyClass::queryIt($sql2, false);

          }

          public static function getIngredientiFromBase(String $base)
          {

          }
//-------------------------------------------------------------------------
          public static function addIngrediente(String $ingrediente)
          {
            $conn = MyClass::login();
            $sql = "INSERT INTO `ingrediente` (`nome`) VALUES ('$ingrediente')";
            if ($conn->query($sql) === true) {
              return true;
            } else {
              return false;
            }
          // RETURN TRUE O FALSE in base al risultato della query
          /*
          con le query insert e update e delect
          il risultato della query è TRUE se la query va a buon fine altrimenti stampa l'errore
          così fai prima a ceccare il risultato
            if ($conn->query($sql) === TRUE) {
              //query andata a buon finr
            } else {
              //false
            }
             */
          }

        //collegare ingrediente ed allergene
          public static function addIngredienteAllergene(String $ingrediente, int $allergene)
          {
            $conn = MyClass::login();
            $sql = "INSERT INTO ingrediente_allergene (allergeneID, nome) VALUES ('$allergene', '$ingrediente')";
            if ($conn->query($sql) === true) {
              $conn->close();
              return true;
            } else {
              $conn->close();
              return false;
            }
          }

          public static function addBase(String $nomebase)
          {
            $conn = MyClass::login();
            $sql = "INSERT INTO `base` (`id`, `nome`) VALUES (NULL, '$nomebase')";
            if ($conn->query($sql) === true) {
              $last_id = $conn->insert_id;
              $conn->close();
              return $last_id;
            } else {
              return false;
            }
          }

          public static function addBaseIngrediente(String $idbase, String $ingrediente)
          {
            $conn = MyClass::login();
            $sql = "INSERT INTO `base_ingrediente` (`baseID`, `nome`) VALUES ('$idbase', '$ingrediente')";
            if ($conn->query($sql) === true) {
              $conn->close();
              return true;
            } else {
              $conn->close();
              return false;
            }
          }

          public static function addPiatto(String $nome)
          { //si crea automaticamente il piattoID
            $conn = MyClass::login();
            $sql = "INSERT INTO piatto (nomePiatto) VALUES ('$nome')";
            if ($conn->query($sql) === true) {
              $last_id = $conn->insert_id;
              $conn->close();
              return $last_id;
            } else {
              $conn->close();
              return false;
            }
          }

          public static function addPiattoBase(int $idpiatto, int $base)
          {// si geneera piatto_base
            $conn = MyClass::login();

            $sql = "INSERT INTO piatto_base (piattoID, baseID) VALUES ('$idpiatto', '$base')";
            if ($conn->query($sql) === true) {
              $conn->close();
              return true;
            } else {
              $conn->close();
              return false;
            }
          }

        //crea la farcitura dato un piatto e un ingrediente
          public static function addPiattoFarcitura(int $idpiatto, String $ingrediente)
          {//si genera farcitura
            $conn = MyClass::login();
            $sql = "INSERT INTO farcitura (piattoID, nome) VALUES ('$idpiatto', '$ingrediente')";
            if ($conn->query($sql) === true) {
              $conn->close();
              return true;
            } else {
              $conn->close();
              return false;
            }
          }

          public static function addPiattoRistorante(int $idpiatto, int $idristo, float $prezzo)
          {
            $conn = MyClass::login();
            $sql = "INSERT INTO `piatto_ristorante`(`costo`,`piattoID`,`ristoID`) VALUES ('$prezzo', '$idpiatto', '$idristo')";
            if ($conn->query($sql) === true) {
              $conn->close();
              return true;
            } else {
              $conn->close();
              return false;
            }
          }
        }
