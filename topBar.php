<?php
session_start();
include 'static.php';
function active($var)
{
    if ((isset($_SESSION["page"]) && $_SESSION["page"] == $var)) {
        return ' class = "active" ';
    } else {
        return ' class = "inActive" ';
    }
}

function topBarButton($var, $name, $tooltip, $param = "")
{
    echo '<a href="javascript:void(0);" data-toggle="tooltip" title="'.$tooltip.'" onclick=\'myload("' . $var . '")\' ' . active($var) .' ' . $param . ' >' . $name . '</a>';
    return;
}

function init()
{
    topBarButton("home.html", "<i class='material-icons'>home</i>", "home");
    if (isset($_SESSION["nickname"]) && MyClass::getTipologiaFromNick($_SESSION["nickname"]) == 'compratore') {
        topBarButton("RestourantView.html", "<i class='material-icons'>store</i>", "ristoranti");
    }
    if (!isset($_SESSION["nickname"])) {
        topBarButton("RestourantView.html", "<i class='material-icons'>store</i>", "ristoranti");
        topBarButton("login.html", "<i class='material-icons'>input</i>", "login");
    } else if (isset($_SESSION["nickname"])) {
        topBarButton("profilo.html", "<i class='material-icons'>face</i>", "profilo");
    }
    if (isset($_SESSION["nickname"]) && MyClass::getTipologiaFromNick($_SESSION["nickname"]) == 'compratore') {
        topBarButton("carrello.html", "<i class='material-icons'>shopping_cart</i>", "carrello");
    }
    topBarButton("info.html", "<i class='material-icons'>description</i>", "informazioni");
    topBarButton("notify.html", "<i class='material-icons'>notification_important</i>", "notifiche", "id='notifi' style='display:none;'");
    echo '<a href="javascript:void(0);" class="icon" data-toggle="tooltip" title="menu" onclick="myFunction()"><i class="material-icons">menu</i></a>';
    if (isset($_SESSION["tipologia"])) {
        switch ($_SESSION["tipologia"]) {
            case 'Admin':
                topBarButton("amministratore.html", '<i class="material-icons">bar_chart</i>', "opzioni");
                break;
            case 'fattorino':
                topBarButton("fattorino.html", '<i class="material-icons">bar_chart</i>', "opzioni");
                break;
            case 'ristoratore':
                topBarButton("ristoratore.html", '<i class="material-icons">bar_chart</i>', "opzioni");
                break;
            default:
                # empty...
            break;
        };

    }
    if (isset($_SESSION["nickname"])) {
        echo '<a href="javascript:void(0);" data-toggle="tooltip" title="uscita" onclick="logOut()" class="inActive"><i class="material-icons">exit_to_app</i></a>';
    }
}

if (isset($_POST["init"]) && $_POST["init"] == "init") {
    init();
} else if (isset($_POST["actual"])) {
    if ((isset($_SESSION["page"]))) {
        echo $_SESSION['page'];
    } else {
        $_SESSION['page'] = 'home.html';
        echo 'home.html';
    }
} else if (isset($_POST["setPage"])) {
    $_SESSION["page"] = $_POST["setPage"];
    echo $_SESSION['page'];
}

?>
