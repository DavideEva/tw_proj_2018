<?php
session_start();
include 'static.php';

function getName(int $id)
{
  $sql = "SELECT * FROM ristorante WHERE id = $id";
  $result = MyClass::queryIt($sql, false);
  if ($result != null) {
    $row = $result->fetch_assoc();
  }
  echo $row['nome'];
  return;
}

function updatePiatti(int $id)
{
  $sql = "SELECT `piatto`.nomePiatto, `piatto`.piattoID, `piatto_ristorante`.costo  FROM ristorante
              JOIN `piatto_ristorante` ON `piatto_ristorante`.ristoID=`ristorante`.id
              JOIN `piatto` ON `piatto`.piattoID=`piatto_ristorante`.piattoID
              WHERE `ristorante`.id = $id";

  $result = MyClass::queryIt($sql, false);

  if ($result !== null && $result->num_rows >= 1) {
    while ($row = $result->fetch_assoc()) {
      $internID = $row['piattoID'];
      $sql = "SELECT  `farcitura`.nome FROM  `piatto`
                  JOIN  `farcitura` ON  `piatto`.piattoID = `farcitura`.piattoID
                  WHERE `piatto`.piattoID = $internID";
      $internquery = MyClass::queryIt($sql, false);
      $stringq = MyClass::fetch_allToString($internquery->fetch_all(), false);
      $sql2 = "SELECT DISTINCT `allergene`.`allergeneID`  FROM  `piatto`
                  JOIN  `farcitura` ON  `piatto`.piattoID = `farcitura`.piattoID
                  JOIN  `piatto_base` ON `piatto`.piattoID = `piatto_base`.piattoID
                  JOIN  `base_ingrediente` ON `base_ingrediente`.`baseID` = `piatto_base`.baseID
                  JOIN  `ingrediente_allergene` ON (`ingrediente_allergene`.nome = `farcitura`.nome OR
                     																`ingrediente_allergene`.nome = `base_ingrediente`.nome)
                  JOIN  `allergene` ON `allergene`.`allergeneID` = `ingrediente_allergene`.`allergeneID`
                  WHERE `piatto`.piattoID = $internID";
      $internquery2 = MyClass::queryIt($sql2, false);
      $allergeni = MyClass::fetch_allToString($internquery2->fetch_all(), false);
      MyClass::createTripleStringButton($row['nomePiatto'], $stringq, $allergeni, $row['costo'], $id, $row['piattoID']);
    }
  } else {
    echo "0 results";
  }
  return;
}

if (isset($_GET['funct']) && isset($_GET['restID'])) {
  if ($_GET['funct'] == 1) {
    getName($_GET['restID']);
  } else if ($_GET['funct'] == 2) {
    updatePiatti($_GET['restID']);
  }
} else if (isset($_GET['funct']) && $_GET['funct'] == 3) {
  if (isset($_SESSION['nickname']) && MyClass::getTipologiaFromNick($_SESSION['nickname']) === 'compratore') {
    echo 'ok';
  } else if (!isset($_SESSION['nickname'])) {
    echo 'fail';
  } else {
    echo 'utentesbagliato';
  }
}

?>
