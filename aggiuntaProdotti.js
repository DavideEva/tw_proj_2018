function addingrediente() {
    allergeni = $("#inputAllergeni").val() != '' ? $("#inputAllergeni").val() : new Array();
    ingrediente = document.getElementById("ingredienteid").value;
    if (ingrediente === '') {
        alert("Mancato ingrediente");
        return;
    }
    $.post("aggiuntaAllergeni.php", {
        ingrediente : ingrediente,
        allergeni : allergeni
    },
        function (data) {
            if (data === 'ok') {
                location.reload();
            } else {
                alert("fail " + data);
            }
        }
    );
}

function addBase() {
    ingredienti = $("#inputIngredienti").val();
    base = document.getElementById("baseid").value;
    console.log(base);
    if (base === '') {
        alert("Mancata base");
        return;
    }
    if (ingredienti.length > 0) {

        $.post("aggiuntaAllergeni.php", {
            base : base,
            ingredienti : ingredienti
    },
    function (data) {
        if (data === 'ok') {
            location.reload();
        } else {
            alert("fail" + data);
        }
    }
    ); 
    } else {
        alert("mancano gli ingredienti!");
    }
}

function addPiatto() {
    ingredienti = $("#inputIngredienti2").val();
    base = $("#inputBase").val();
    piatto = $("#piattoid").val();
    console.log(piatto);
    if (base === '') {
        alert("Mancante base");
        return;
    } else if (piatto === '') {
        alert("Mancante piatto");
        return;
    }
    if (ingredienti.length > 0) {
        $.post("aggiuntaAllergeni.php", {
            baseID : base,
            ingredienti : ingredienti,
            nomePiatto : piatto
        },
        function (data) {
            if (data === 'ok') {
                location.reload();
            } else {
                alert("fail" + data);
            }
        }
        ); 
    } else {
        alert("mancano gli ingredienti!");
    }
}

function addPiattoInRistorante() {
    idpiatto = $("#idpiatto").val();
    idristo = $("#idRisto").val();
    costo = $("#prezzo").val();
    console.log(idpiatto, idristo, costo);
    if (idpiatto === '') {
        alert("Mancante base");
        return;
    } else if (idristo === '') {
        alert("Mancante piatto");
        return;
    } else if (costo === '') {
        alert("Mancante piatto");
        return;
    } 
    $.post("aggiuntaAllergeni.php", {
        idpiatto : idpiatto,
        idristo : idristo,
        costo : costo
    },
        function (data) {
            if (data === 'ok') {
                location.reload();
            } else {
                alert("fail" + data);
            }
        }
    ); 
}


$(document).ready(function () {
    $.getJSON("ristoratore.php?request=allallergeni", function(data) {
        for (var i = 0; i < data.length; i++)
        {
            local_id = data[i]["allergeneID"];
            local_nome = data[i]["nome"];
            $("#inputAllergeni").append("<option value='"+local_id+"'>"+local_nome+"</option>")
        }
    });

    $.getJSON("ristoratore.php?request=ingredienti", function(data) {
        for (var i = 0; i < data.length; i++)
        {
            local_nome = data[i]["nome"];
            $("#inputIngredienti").append("<option value='" + local_nome + "'>"+local_nome+"</option>")
            $("#inputIngredienti2").append("<option value='" + local_nome + "'>"+local_nome+"</option>")
        }
    });

    $.getJSON("ristoratore.php?request=basi", function(data) {
        for (var i = 0; i < data.length; i++)
        {
            local_id = data[i]["id"];
            local_nome = data[i]["nome"];
            $("#inputBase").append("<option value='"+local_id+"'>"+local_nome+"</option>")
        }
    });

    $.getJSON("ristoratore.php?request=course", function(data) {
        for (var i = 0; i < data.length; i++)
        {
            local_id = data[i]["piattoID"];
            local_nome = data[i]["nomePiatto"];
            $("#idpiatto").append("<option value='"+local_id+"'>"+local_nome+"</option>");
        }
    });

    $.getJSON("ristoratore.php?request=ristoranti", function(data) {
        for (var i = 0; i < data.length; i++)
        {
            local_id = data[i]["id"];
            local_nome = data[i]["nome"];
            $("#idRisto").append("<option value='"+local_id+"'>"+local_nome+"</option>");
        }
    });
    
});