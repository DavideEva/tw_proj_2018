$(document).ready(function () {
    $("form button").click(function () {
        event.preventDefault();
        errors = "";

        var nickname = $("input#inputNickname").val();
        var password = $("input#inputPassword").val();

        if (nickname == null) {
            errors += "Nickname mancante! ";
        }

        if (password == null) {
            errors += "Password mancante! ";
        }

        if (errors.length > 0) {
            alert( "Data Loaded: " + errors);
        } else {
            var posting = $.post("loginCheck.php", {
                make: "login",
                username: nickname,
                password: password,
            })
            posting.done(function (msg) {
                if (msg === 'ok') {
                    myload("home.html", true);
                } else {
                    $("#result").html(msg);
                    $("#result").show();
                }
            });
        }

    });
});
