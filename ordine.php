<?php
  include 'static.php';
  session_start();
  if(isset($_SESSION['nickname']) && isset($_GET['funct'])){
    if($_GET['funct'] == "add" && isset($_GET['restID']) && isset($_GET['quantita']) && isset($_GET['piattoID'])){
      addToCart($_GET['restID'],$_GET['quantita'],$_GET['piattoID'],$_SESSION['nickname']);
    } else if ($_GET['funct'] == "showCart"){
      showCart($_SESSION['nickname']);
    } else if ($_GET['funct'] == "sendCart" && isset($_GET['time']) && isset($_GET['luogo'])){
      sendCart($_SESSION['nickname'], $_GET['time'], $_GET['luogo']);
    } else if ($_GET['funct'] == "time"){
      maxTime($_SESSION['nickname']);
    } else if ($_GET['funct'] == "howManyNotify"){
      notify($_SESSION['nickname']);
    } else if ($_GET['funct'] == "removeNelem" && isset($_GET['ordine']) && isset($_GET['email']) && isset($_GET['piatto'])&& isset($_GET['risto'])&& isset($_GET['num'])){
      removeNelem($_GET['ordine'], $_GET['email'],$_GET['piatto'],$_GET['risto'],$_GET['num']);
    } else if ($_GET['funct'] == "showAccepted"){
      showAccepted($_SESSION['nickname']);
    }
  } else if (isset($_POST['ordine']) && isset($_POST['email']) && isset($_POST['piatto']) && isset($_POST['risto'])) {
    removeFromCart($_POST['ordine'],$_POST['email'],$_POST['piatto'],$_POST['risto']);
  }


function showAccepted(string $nick){
  $email = MyClass::getEmailFromNick($nick);
  $sql = "SELECT `utente`.tipologia FROM `utente`
          WHERE `utente`.nickname='$nick'";
  $result = MyClass::queryIt($sql,false);
  if ($result!==null && $result->num_rows == 1) {
    $strtipo = $result->fetch_assoc()['tipologia'];
    if ($strtipo == 'ristoratore'){
        $sql2 = "SELECT `ordine`.id, `ristorante`.id AS ristoID, `ristorante`.nome, `piatto`.nomePiatto, `ordine`.quantita, `ordine`.orario FROM `ordine`
                 JOIN `ristorante` ON `ordine`.ristoID = `ristorante`.id
                 JOIN `utente` ON `ristorante`.gestoreID = `utente`.email
                 JOIN `piatto` ON `ordine`.piattoID = `piatto`.piattoID
                 WHERE `utente`.email = '$email'
                 AND `ordine`.tipologiaOrdine = 'ACCETTATO'
                 ORDER BY `ordine`.id";
        $resultinner = MyClass::queryIt($sql2,false);

        if ($resultinner !== null && $resultinner->num_rows>= 1){
          ?>
          <h2>ordini accettati</h2>
          <?php
          $lastID = 0;
          while($row = $resultinner->fetch_assoc()){
            if ($row['id'] > $lastID){
              if ($lastID != 0){
                ?></span></div><?php
              }
              $lastID = $row['id'];
              ?><div id="div_ordine<?=$row['id']?>"><span>il ristorante <?=$row['nome']?> ha un ordine per il <?=$row['orario']?></span>
                <button onclick="evaso(<?=$row['id']?>,<?=$row['ristoID']?>)">evaso</button>
              <?php

            }
            ?>
            <div><?=$row['quantita']?>x <?=$row['nomePiatto']?></div>
            <?php
          }
          ?></div>
          <?php
        }
      }
    }
}

  function removeNelem(String $ordineID, String $emailogged, String $piattoID, String $ristoID, int $num){
    $sql = "SELECT `ordine`.quantita FROM `ordine`
            JOIN `utente` ON `utente`.email = `ordine`.compratoreID
            WHERE `ordine`.tipologiaOrdine = 'CARRELLO'
            AND `ordine`.id = '$ordineID'
            AND `ordine`.compratoreID = '$emailogged'
            AND `ordine`.piattoID = '$piattoID'
            AND `ordine`.ristoID = '$ristoID'";
            $result=MyClass::queryIt($sql,false);
    if ($result !== null && $result->num_rows == 1){
      $quantita = $result->fetch_assoc()['quantita'];
      if ($quantita > $num){
        $newquant = $quantita - $num;
        $sql2 = "UPDATE `ordine`
                SET quantita = $newquant
                WHERE `ordine`.tipologiaOrdine = 'CARRELLO'
                AND `ordine`.id = '$ordineID'
                AND `ordine`.compratoreID = '$emailogged'
                AND `ordine`.piattoID = '$piattoID'
                AND `ordine`.ristoID = '$ristoID'";
        if (MyClass::updateIt($sql2,false) === TRUE) {
          echo 'ok';
        } else {
          echo 'fail';
        }
      } else {
        return;
        removeFromCart($ordineID, $emailogged, $piattoID, $ristoID);
      }
    }
  }

  function removeFromCart(String $ordineID, String $emailogged, String $piattoID, String $ristoID){
    $conn = MyClass::login();
    $sql = "DELETE FROM `ordine`
            WHERE `ordine`.tipologiaOrdine = 'CARRELLO'
            AND `ordine`.id = '$ordineID'
            AND `ordine`.compratoreID = '$emailogged'
            AND `ordine`.piattoID = '$piattoID'
            AND `ordine`.ristoID = '$ristoID'";
    if (($conn->query($sql) === TRUE)) {
      echo 'ok';
    } else {
      echo 'fail';
    }
  }

  function addToCart(int $restID, int $quantita, string $piattoID, string $nick){
  $emailogged = MyClass::getEmailFromNick($nick);
  $sql = "SELECT `ordine`.tipologiaOrdine, `ordine`.compratoreID,
                 `ordine`.piattoID, `ordine`.ristoID, `ordine`.id, `ordine`.quantita
                 FROM `ordine`
          JOIN `utente` ON `ordine`.compratoreID = `utente`.email
          WHERE `utente`.nickname='$nick'
          AND `ordine`.tipologiaOrdine = 'CARRELLO'";
  $result = MyClass::queryIt($sql,true);
  $stillToDo=true;
  $ordid;
  if ($result !== null && $result->num_rows >= 1){
    while ($stillToDo && ($row = $result->fetch_assoc())){
      $ordid = $row['id'];
      if ($row['piattoID'] == $piattoID   //idpiatto          //QUESTO PUO ESSERE SOLO UN ELSE ...?
              && $row['ristoID'] == $restID) {
        $newquantita = $row['quantita']+$quantita;
        $sqlinsert = "UPDATE `ordine`
                      SET `quantita`= $newquantita
                      WHERE `ordine`.compratoreID = '$emailogged'
                      AND `ordine`.piattoID = $piattoID
                      AND `ordine`.ristoID = $restID
                      AND `ordine`.id = $ordid
                      AND `ordine`.tipologiaOrdine = 'CARRELLO'"; //id ordine
        MyClass::updateIt($sqlinsert,true);
        $stillToDo=false;
      }
    }
    if ($stillToDo){
        $sqlinsert = "INSERT INTO `ordine` (`id`, `compratoreID`, `piattoID`, `quantita`, `ristoID`, `tipologiaOrdine`)
                      VALUES ($ordid, '$emailogged', '$piattoID', '$quantita', '$restID', 'CARRELLO');";
                      MyClass::updateIt($sqlinsert,true);
    }
  } else {
      $sqlinsert = "INSERT INTO `ordine` (`id`, `compratoreID`, `piattoID`, `quantita`, `ristoID`, `tipologiaOrdine`)
                    VALUES ('', '$emailogged', '$piattoID', '$quantita', '$restID', 'CARRELLO');";
      MyClass::updateIt($sqlinsert,true);
  }
}

function showCart(string $nick){
  $emailogged = MyClass::getEmailFromNick($nick);
  $result = MyClass::findCart($nick);
  if ($result->num_rows >= 1) {
    $prezzofinale = 0.0;
    while($row = $result->fetch_assoc()){
      $piattoID = $row['piattoID'];
      $ristoID = $row['ristoID'];
      $ordineID = $row['id'];
      $sql2 = "SELECT  `piatto_ristorante`.costo, `piatto`.nomePiatto  FROM  `piatto_ristorante`
                JOIN `piatto` ON `piatto`.piattoID = `piatto_ristorante`.piattoID
                WHERE `piatto_ristorante`.piattoID = $piattoID
                AND `piatto_ristorante`.ristoID = $ristoID";
      $internquery = MyClass::queryIt($sql2,false);
      $piatto = $internquery->fetch_assoc();
      $prezzotot = $row['quantita'] * $piatto['costo'];
      $quantitaStr = $row['quantita'] > 1 ? $row['quantita'].' * ' : '';
      $str1 = $quantitaStr.$piatto['nomePiatto'];
      $sql3 = "SELECT  `ristorante`.nome FROM  `ristorante`
                WHERE `ristorante`.id = $ristoID";
      $internquery2 = MyClass::queryIt($sql3,false);
      $nomeristo = MyClass::fetch_allToString($internquery2->fetch_all(),false);
      MyClass::createCartButton($str1, $nomeristo, $prezzotot, $ordineID, $emailogged, $piattoID, $ristoID, $row['quantita']);
      $prezzofinale += $prezzotot;
    }
    ?><span class="badge badge-info" id="prezzofinale">spesa totale = <?=$prezzofinale?>€</span><?php
  } else {
    ?><span class="badge badge-warning" id="carrello_vuoto">il carrello è vuoto :(</span><?php
  }
}

function sendCart(string $nick, string $time, string $luogo){
  $emailogged = MyClass::getEmailFromNick($nick);
  $pm = substr($time, -2);
  $time = substr($time, 0, -2);
  $dt = DateTime::createFromFormat("H:i", $time);
  date_default_timezone_set('Europe/Berlin');
  $datestr = date('m/d/Y h:i:s a', time());
  $date=date_create($datestr);
  $diff=date_diff($date,$dt,false);
  if ($diff->invert){
    date_add($dt,date_interval_create_from_date_string("1 day"));
  }
  if ($pm == 'pm'){
    var_dump($dt);
    date_add($dt,date_interval_create_from_date_string("12 hours"));
    var_dump($dt);
  }
  $result = MyClass::findCart($nick);
  $datasql = $dt->format('Y-m-d H:i:s');
  if ($result->num_rows >= 1) {
    $sqlinsert = "UPDATE `ordine`
                  SET `tipologiaOrdine`= 'INVIATO', `orario`= '$datasql', `luogo`= '$luogo'
                  WHERE `ordine`.tipologiaOrdine = 'CARRELLO'
                  AND `ordine`.compratoreID = '$emailogged'";
    MyClass::updateIt($sqlinsert,false);
  }
}

function maxTime(string $nick){
  $emailogged = MyClass::getEmailFromNick($nick);
  $sql = "SELECT DISTINCT `ristorante`.tempoConsegnaMin  FROM  `ristorante`
            JOIN `ordine` ON `ordine`.ristoID = `ristorante`.id
            WHERE `ordine`.compratoreID = '$emailogged'
            AND `ordine`.tipologiaOrdine = 'CARRELLO'";
  $result = MyClass::queryIt($sql,false);
  if ($result!==null && $result->num_rows >= 1) {
    $max = 0;
    foreach($result->fetch_all() as $value){
      $max = $value[0] > $max? $value[0] : $max;
    }
    echo $max;
  }
}

function notify(string $nick){
  $email = MyClass::getEmailFromNick($nick);
  $sql = "SELECT `utente`.tipologia FROM `utente`
          WHERE `utente`.nickname='$nick'";
  $result = MyClass::queryIt($sql,false);
  if ($result!==null && $result->num_rows == 1) {
    $strtipo = $result->fetch_assoc()['tipologia'];
    $sql2="";
    switch($strtipo){
    case 'ristoratore':
        $sql2 = "SELECT DISTINCT `ordine`.id FROM `ordine`
                 JOIN `ristorante` ON `ordine`.ristoID = `ristorante`.id
                 JOIN `utente` ON `ristorante`.gestoreID = `utente`.email
                 WHERE `utente`.email = '$email'
                 AND `ordine`.tipologiaOrdine = 'INVIATO'";
        break;
    case 'fattorino':
        $sql2 = "SELECT DISTINCT `ordine`.id FROM `ordine`
                 JOIN `utente` ON `ordine`.fattorinoID = `utente`.email
                 WHERE `ordine`.fattorinoID='$email'
                 AND `ordine`.tipologiaOrdine = 'ACCETTATO'";
        break;
    case 'Admin':
        //code to be executed if n=label3;
        break;
    case 'compratore':
        $sql2 = "SELECT DISTINCT `ordine`.id FROM `ordine`
                 JOIN `utente` ON `ordine`.compratoreID = `utente`.email
                 WHERE `ordine`.compratoreID='$email'
                 AND (`ordine`.tipologiaOrdine = 'ACCETTATO'
                 OR `ordine`.tipologiaOrdine = 'EVASO')";
        break;
    default:
        echo "error";
        return;
    }
    $resultinner = MyClass::queryIt($sql2,false);
    echo $resultinner->num_rows;
  }
}
?>
