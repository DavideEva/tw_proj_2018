<?php
session_start();
include 'static.php';
header('Content-Type: application/json');

if (isset($_GET['request']) && $_GET['request'] == 'ristoranti') {

    $conn = MyClass::login();
    $sql = "SELECT `ristorante`.id, `ristorante`.nome FROM `ristorante`";
    $result = MyClass::queryIt($sql, false);
    while ($row = $result->fetch_assoc()) {
        $output[] = $row;
    }
    print json_encode($output);
    return;
} else if (isset($_POST['ristup']) && isset($_SESSION['nickname']) && MyClass::getTipologiaFromNick($_SESSION['nickname']) == 'fattorino') {
    $nick = $_SESSION['nickname'];
    $ristID = $_POST['ristup'];
    $sql = "UPDATE `utente` SET `ristoID`=$ristID WHERE `nickname`='$nick'";
    MyClass::updateIt($sql, false);
} else if (isset($_SESSION['nickname']) && MyClass::getTipologiaFromNick($_SESSION['nickname']) == 'fattorino' && isset($_GET['request']) && $_GET['request'] == 'miorist') {
    $nick = $_SESSION['nickname'];
    $conn = MyClass::login();
    $sql = "SELECT `ristorante`.id, `ristorante`.nome FROM `ristorante` JOIN `utente` ON `utente`.ristoID = `ristorante`.id
    where `utente`.nickname = '$nick'
    AND `utente`.tipologia = 'fattorino'";
    $result = MyClass::queryIt($sql, false);
    while ($row = $result->fetch_assoc()) {
        $output[] = $row;
    }
    print json_encode($output);
    return;
}
 

?>