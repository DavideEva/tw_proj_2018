function updateOrdine() {
  xmlhttp=preparexmlobj();
  xmlhttp.onreadystatechange=function() {
      if (this.readyState==4 && this.status==200) {
          document.getElementById("div_ord").innerHTML=this.responseText;
      }
  }
  xmlhttp.open("GET","ordine.php?funct=showAccepted",true);
  xmlhttp.send();
  return ;
}

function evaso(ordineid,ristoID){
  xmlhttp=preparexmlobj();
  xmlhttp.onreadystatechange=function() {
      if (this.readyState==4 && this.status==200) {
        if (this.responseText >= 1){
          parent = $("#div_ordine"+ordineid).parent();
          document.getElementById("div_ordine"+ordineid).remove();
          if (parent.children()[1] == undefined){
            window.location.href = 'home.html';
          }
        }
      }
  }
  xmlhttp.open("GET","notify.php?funct=ordineEvaso&ordineid="+ordineid+"&ristoID="+ristoID,true);
  xmlhttp.send();
  return ;
}

$(document).ready(function () {
  updateOrdine();
});
