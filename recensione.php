<?php
    session_start();
    include 'static.php';
    function loadR(int $id)
    {
        $conn = MyClass::login();
        $sql = "SELECT * FROM recensione WHERE ristoID='$id'";
        $result = $conn->query($sql);
        if ($result !== false)
        foreach ($result as $value) {
            printRecensioneBootstrap(MyClass::getNickFromEmail($value['compratoreID']), $value['compratoreID'], $value['stelle'], $value['descrizione'], MyClass::getFotoFromEmail($value['compratoreID']));
        }

        $conn->close();
    }

    function printStar(int $var)
    {
        $res = '<small class="col-md-6 float-right"> valutazione: ';
        for ($i = 0; $i < 5; $i++) {
            if ($i < $var)
                $res .= '<i class="fa fa-rocket text-warning"></i>';
            else
                $res .= '<i class="fa fa-rocket"></i>';
        }
        $res .= '</small>';
        return $res;
    }

    function printRecensioneBootstrap(String $nick, String $compratore, int $stelle, String $descrizione, $foto = "")
    {
        $src = "";
        if (!empty($foto)) {
            $src = '<img src="'.$foto.'" alt="'.$nick.'" class="mr-3 mt-3 rounded-circle" style="width:60px;">';
        }

        echo '<div class="media border p-3">'.$src.'

        <div class="media-body">
          <h4><a href="#" data-toggle="tooltip" title="'.$compratore.'">'.$nick.'</a>'.printStar($stelle).'</h4>
          <p>Descrizione: '.$descrizione.'</p>
        </div>
      </div>';
    }

    function uploadRecensione(int $restID, String $user, int $star, String $recensione = "")
    {
        $conn = MyClass::login();
        $sql = "SELECT * FROM recensione WHERE compratoreID='$user' AND ristoID='$restID'";
        $result = $conn->query($sql);
        if (empty($result)) {
            $sql3 = "INSERT INTO recensione(compratoreID, ristoID, stelle, descrizione) VALUES ('$user', '$restID', '$star', '$recensione')";
            if ($conn->query($sql3) == TRUE) {
                echo 'ok';
            } else {
                echo 'error';
            }
        } else {
            $sql2 = "UPDATE recensione SET stelle='$star', descrizione='$recensione' WHERE `recensione`.compratoreID='$user' AND `recensione`.ristoID='$restID'";
            if ($conn->query($sql2) == TRUE) {
                echo 'update';
            } else {
                echo 'errorupdate';
                echo mysqli_error($conn);
            }
        }
        $conn->close();
    }

    if (isset($_POST['load'])) {
        loadR($_POST["load"]);
    } else if (isset($_POST["upload"]) && isset($_SESSION['nickname'])) {
        uploadRecensione($_POST["upload"], MyClass::getEmailFromNick($_SESSION['nickname']), $_POST["star"], $_POST["desc"]);
    }

?>
