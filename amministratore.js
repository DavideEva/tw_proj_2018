function eliminaUser(id){
  $.post("eliminazione.php",{
    state : "user",
    id : id
  }, function(data){
    if (data == "ok"){
      alert("eliminazione terminata con successo!");
      location.reload();
    } else {
      alert("eliminazione non terminata");
    }
  });
}

function eliminaRisto(id){
  $.post("eliminazione.php",{
    state : "risto",
    id : id
  }, function(data){
    if (data == "ok"){
      alert("eliminazione terminata con successo!");
      location.reload();
    } else {
      alert("eliminazione non terminata");
    }
  });
}

function eliminaAllergen(id){
  $.post("eliminazione.php",{
    state : "allergen",
    id : id
  }, function(data){
    if (data == "ok"){
      alert("eliminazione terminata con successo!");
      location.reload();
    } else {
      alert("eliminazione non terminata");
    }
  });
}

function manage_pages(current_page, num_pages, id){

	$("nav#" + id + " ul li").removeClass("disabled");
	$("nav#" + id + " ul li").removeClass("active");

	if(current_page ==0){
		$("nav#" + id + " ul li:first-child").addClass("disabled");
	}

	if(current_page == num_pages - 1){
		$("nav#" + id + " ul li:last-child").addClass("disabled");
	}

	$("nav#" + id + " ul li:nth-child(" + (current_page + 2) + ")").addClass("active");
}

function getFromDB(id, page) {
  $.getJSON("amministratore.php?request=" + id + "&page=" + page, function(data) {
		var html_code = "";
		for(var i = 0; i < data.length; i++){
      if (id === "users") {
        html_code += '<tr><td class="nome">'+data[i]["nome"]+'</td><td class="cognome">'+data[i]["cognome"]+'</td><td class="email">'+data[i]["email"]+'</td><td class="bottone"><input type="button" value="elimina" id="button-risto" onclick="eliminaUser(\'' + data[i]["nickname"] +'\');"></td></tr>';
      } else if (id === "restaurants") {
        html_code += '<tr><td class="nome">'+data[i]["nome"]+'</td><td class="email">'+data[i]["email"]+'</td><td class="gestoreID">'+data[i]["gestoreID"]+'</td><td class="bottone"><input type="button" value="elimina" id="button-risto" onclick="eliminaRisto(' + data[i]["id"] +');" ></td></tr>';
      } else if (id === "allergens") {
        html_code += '<tr><td class="allergeneID">'+data[i]["allergeneID"]+'</td><td class="nome">'+data[i]["nome"]+'</td><td class="bottone"><input type="button" value="elimina" id="button-risto" onclick="eliminaAllergen(' + data[i]["allergeneID"] +');" ></td></tr>';
      }
		}

		$("table#" + id + " tbody").html(html_code);
	});
}

function setUp(id, page) {
  $.getJSON("amministratore.php?request=" + id + "&num_pages", function(data) {
    var num_pages = data["num_pages_" + id ];

    html_code = '<li class="page-item disabled"><a class="page-link" href="#">Prev</a></li>';
    for(let i = 0; i < num_pages; i++){
      html_code += '<li class="page-item"><a class="page-link" href="#">'+ (i+1) + '</a></li>';
    }
    html_code += '<li class="page-item"><a class="page-link" href="#">Next</a></li>';

    $("nav#" + id + " ul").html(html_code);

    manage_pages(page, num_pages, id);

    $("nav#"+ id + " ul li").click(function(){
      //controllo se ha classe active o disabled
      if(!$(this).hasClass("disabled") && !$(this).hasClass("active")){
        var contenuto = $(this).find("a").text();
        switch(contenuto) {
          case "Prev":
            page -= 1;
            break;
          case "Next":
            page += 1;
            break;
          default:
            page = contenuto - 1;
        }

        getFromDB(id, page);
        manage_pages(page, num_pages, id);
      }

    });

  });
}

function addallergene(allergene) {
  val = document.getElementById(allergene).value;
  console.log(val);
  $.post("aggiuntaAllergeni.php", {
    allergene : val
  },
    function (data) {
      if (data == 'ok') {
        location.reload();
      } else {
        alert('Errore!');
      }
    }
  );
}

$(document).ready(function(){
	var page = 0;
  var id = "users";

  getFromDB(id, page);
  setUp(id, page);

  id = "restaurants";

  getFromDB(id, page);
  setUp(id, page);

  id = "allergens";

  getFromDB(id, page);
  setUp(id, page);

  $("#piattiadd").load("aggiuntaProdotti.html");
});
