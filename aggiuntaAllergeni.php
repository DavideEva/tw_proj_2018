<?php
session_start();
include 'static.php';
if (isset($_SESSION['nickname']) && MyClass::getTipologiaFromNick($_SESSION['nickname']) === 'Admin' && isset($_POST['allergene'])) {
    $conn = MyClass::login();
    $allergene = $_POST['allergene'];//SINGOLARE
    $sql = "INSERT INTO `allergene`(`allergeneID`,`nome`) VALUES (NULL,'$allergene')";
    if ($conn->query($sql) === true) {
        echo 'ok';
    } else {
        echo 'fail';
    }
} else if (isset($_SESSION['nickname']) && (MyClass::getTipologiaFromNick($_SESSION['nickname']) === 'ristoratore' || MyClass::getTipologiaFromNick($_SESSION['nickname']) === 'Admin')
    && isset($_POST['ingrediente'])) {
    //Aggiunta di un ingrediente
    $ingrediente = $_POST['ingrediente'];//SINGOLARE

    if (!MyClass::addIngrediente($ingrediente)) {
        //Errore nell'aggiunta dell'ingrediente
        echo "impossibile aggiungere l'ingrediente";
        return;
    }
    if (isset($_POST['allergeni'])) {

        $allergeni = $_POST['allergeni'];//PLURALE
        
        $len = count($allergeni);
        for ($i = 0; $i < $len; $i++) {
            if (!MyClass::addIngredienteAllergene($ingrediente, $allergeni[$i])) {
                echo "errore nell'aggiunta dell'allergene =  " . $allergeni[$i] . " all'ingrediente " . $ingrediente;
                return;
            }
        }
    }
    echo 'ok';
} else if (isset($_SESSION['nickname']) && (MyClass::getTipologiaFromNick($_SESSION['nickname']) === 'ristoratore' || MyClass::getTipologiaFromNick($_SESSION['nickname']) === 'Admin')
    && isset($_POST['base'])) {
    //Aggiunta di una base
    $base = $_POST['base'];//SINGOLARE
    $idbase = MyClass::addBase($base);
    if ($idbase === FALSE) {
        //Errore nell'aggiunta dell'base
        echo "impossibile aggiungere l'base";
        return;
    }
    if (isset($_POST['ingredienti'])) {
        $ingredienti = $_POST['ingredienti'];//PLURALE
        $len = count($ingredienti);
        for ($i = 0; $i < $len; $i++) {
            if (!MyClass::addBaseIngrediente($idbase, $ingredienti[$i])) {
                echo "errore nell'aggiunta dell'ingredienti =  " . $ingredienti[$i] . " alla base " . $idbase;
                return;
            }
        }
    }
    echo 'ok';
} else if (isset($_SESSION['nickname']) && (MyClass::getTipologiaFromNick($_SESSION['nickname']) === 'ristoratore' || MyClass::getTipologiaFromNick($_SESSION['nickname']) === 'Admin')
    && isset($_POST['nomePiatto']) && isset($_POST['baseID'])) {
    //aggiunta di un piatto data base e farcitura
    $nomePiatto = $_POST['nomePiatto'];//NOME del piatto
    $id = MyClass::addPiatto($nomePiatto);
    if ($id === false) {
        return;
    }

    $baseID = $_POST['baseID'];
    if (!MyClass::addPiattoBase($id, $baseID)) {
        echo "errore nell'aggiunta della base =  " . $baseID . " al piatto ";
        return;
    }

    $ingredienti = $_POST['ingredienti'];//PLURALE
    $len = count($ingredienti);
    for ($i = 0; $i < $len; $i++) {
        if (!MyClass::addPiattoFarcitura($id, $ingredienti[$i])) {
            echo "errore nell'aggiunta dell'ingrediente =  " . $ingredienti[$i] . " al piatto ";
            return;
        }
    }
    echo 'ok';
} else if (isset($_SESSION['nickname']) && (MyClass::getTipologiaFromNick($_SESSION['nickname']) === 'ristoratore' || MyClass::getTipologiaFromNick($_SESSION['nickname']) === 'Admin')
    && isset($_POST['farcitura']) && isset($_POST['nomePiatto'])) {
    //Aggiunta di un farcitura
    $farcitura = $_POST['farcitura'];//SINGOLARE
    $nomePiatto = $_POST['nomePiatto'];//PLURALE
    if (!MyClass::addPiattoFarcitura($nomePiatto, $farcitura)) {
            //Errore nell'aggiunta dell'farcitura
        echo "impossibile aggiungere la farcitura " . $farcitura . " al piatto " . $nomePiatto;
    }
    echo 'ok';
} else if (isset($_SESSION['nickname']) && ((MyClass::getTipologiaFromNick($_SESSION['nickname']) === 'ristoratore' || MyClass::getTipologiaFromNick($_SESSION['nickname']) === 'Admin'))
    && isset($_POST['idpiatto']) && isset($_POST['idristo'])) {
    $idpiatto = $_POST['idpiatto'];
    $idristo = $_POST['idristo'];
    $prezzo = $_POST['costo'];
    if (!MyClass::addPiattoRistorante($idpiatto, $idristo, $prezzo)) {
        echo 'ipossibile aggiungere il piatto al ristorante';
        return;
    }
    echo 'ok';
}
?>