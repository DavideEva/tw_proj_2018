<?php
include 'static.php';
header('Content-Type: application/json');
    $conn = MyClass::login();
	  $rows_per_page = 5;

    $page = 0;
    if(isset($_GET["page"])){
      $page = $_GET["page"];

      $start_row = $page * $rows_per_page;

      $stmt = $conn->prepare("SELECT * FROM allergene LIMIT ?, ?");
      $stmt->bind_param("ii", $start_row, $rows_per_page);
      $stmt->execute();

      $result = $stmt->get_result();

      $output = array();
      while($row = $result->fetch_assoc()) {
        $output[] = $row;
      }
      $stmt->close();
      print json_encode($output);
    } else if (isset($_GET["num_pages"])) {
      $stmt = $conn->prepare("SELECT COUNT(*) AS num_allergeni FROM allergene");
      $stmt->execute();

      $stmt->bind_result($num_allergens);
      $stmt->fetch();

      $num_pages_allergens = ceil($num_allergens / $rows_per_page);
      $output = array("num_pages_allergens" => $num_pages_allergens);
      print json_encode($output);
    }
?>
