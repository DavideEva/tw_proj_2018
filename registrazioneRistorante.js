var logo_name = [null, null];
var logo_type = [];
var logo_size = [];
var logo_data = [];

function validateEmail(email) {
    var regex = /^(?:[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/;
    return regex.test(email);
}

function registrazioneRistorante(evt) {
    event.preventDefault();
    errors = "";

    var nome = $("input#inputNome").val();
    var descrizione = $("input#inputDescrizione").val();
    var email = $("input#inputEmail").val();
    var logo_grande = $("input#inputLogoGrande").files;
    var nickname = $("input#inputNickname").val();

    if (nome == null || nome.length < 2) {
        errors += "Nome è obbligatorio e deve essere almeno 2 caratteri; ";
    }

    if (descrizione == null || descrizione.length < 2) {
        errors += "Descrizione è obbligatorio e deve essere almeno 2 caratteri; ";
    }

    if (email == null || !validateEmail(email)) {
        errors += "Email è obbligatoria e deve essere valida; ";
    }

    if (logo_size[0] > 50000 || logo_size[1] > 50000) {
        errors += "loghi troppo grandi";
    }
    for (i = 0; i < 2; i++)
        if (logo_name[i] == null) {
            errors += "manca il logo; ";
        }

    if (errors.length > 0) {
        alert("Data Loaded: " + errors);
    } else {
        var fd0 = new FormData();
        lp = "LP_" + nome + logo_type[0];
        fd0.append('filename0', lp);
        lg = "LG_" + nome + logo_type[1]
        fd0.append('data0', logo_data[0]);
        var fd1 = new FormData();
        fd1.append('filename1', lg);
        fd1.append('data1', logo_data[1]);
        tempo = $("#tempo").val().length != 0 ? $("#tempo").val() : 20;
        $.post("registrazioneRistorante.php",{
            filename0: lp,
            filename1: lg,
            data0: logo_data[0],
            data1: logo_data[1],
            name: nome,
            descrizione: descrizione,
            email: email,
            registrazione: 'ristorante',
            tempo : tempo

        },function(data) {
            console.log(data);
            // print the output from the upload.php script
            if (data === 'ok') {
                $(location).attr('href', 'home.html');
            } else if (data == 'permissionFAIL') {
                $(location).attr('href', 'home.html');
            } else if (data === 'fail') {
                alert("Il server ha risposto " + data);
            }
        });
    }
}

function showLogo(evt, tipo = 0) {

    if (evt == null) {
        if (tipo == 0) {
            var files = document.getElementById('inputLogoPiccolo').files; // FileList object.
        } else {
            var files = document.getElementById('inputLogoGrande').files; // FileList object.
        }
    } else {
        var files = evt.dataTransfer.files;
    }

        // files is a FileList of File objects. List some properties.
    var output = [];
    for (var i = 0, f; f = files[i]; i++) {
        output.push('<li><strong>', escape(f.name), '</strong> (', f.type || 'n/a', ') - ',
        f.size, ' bytes, last modified: ',
        f.lastModifiedDate ? f.lastModifiedDate.toLocaleDateString() : 'n/a',
        '</li>');
        logo_name[tipo] = escape(f.name);
        logo_size[tipo] = f.size;

        if (f.type.indexOf("image/") == 0) {
            logo_type[tipo] = "." + f.type.replace("image/", "");
        } else {
            return;
        }

        var reader = new FileReader();
        reader.onload = (function(theFile) {
            return function(e) {
                // Render thumbnail.
                var span = document.createElement('span');
                span.innerHTML = ['<img class="thumb" src="', e.target.result,'" title="', escape(theFile.name), '" style="min-width: 50px; min-height: 40px; max-width: 80px; max-height: 180px; border-style: solid; border-color: gray "/>'].join('');
                if (tipo == 0) {
                    document.getElementById('list0').insertBefore(span, null);
                } else {
                    document.getElementById('list1').insertBefore(span, null);
                }
                logo_data[tipo] = e.target.result;
            };
        })(f);
        reader.readAsDataURL(f);
    }
    if (tipo == 0) {
        document.getElementById('list0').innerHTML = '<ul>' + output.join('') + '</ul>';
    } else {
        document.getElementById('list1').innerHTML = '<ul>' + output.join('') + '</ul>';
    }
}

function handleFileSelect1(evt) {
    evt.stopPropagation();
    evt.preventDefault();
    showLogo(evt, 0);
}

function handleFileSelect2(evt) {
    evt.stopPropagation();
    evt.preventDefault();
    showLogo(evt, 1);
}

function handleDragOver(evt) {
    evt.stopPropagation();
    evt.preventDefault();
    evt.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
}


$(function () {
  // Setup the dnd listeners.
  var dropZone1 = document.getElementById('drop_zone1');
  var dropZone2 = document.getElementById('drop_zone2');

  dropZone1.addEventListener('dragover', handleDragOver, false);
  dropZone1.addEventListener('drop', handleFileSelect1, false);
  dropZone2.addEventListener('dragover', handleDragOver, false);
  dropZone2.addEventListener('drop', handleFileSelect2, false);

});
