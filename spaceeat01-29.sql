-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Gen 29, 2019 alle 11:23
-- Versione del server: 10.1.37-MariaDB
-- Versione PHP: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `spaceeat`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `allergene`
--

CREATE TABLE `allergene` (
  `allergeneID` tinyint(50) NOT NULL,
  `nome` char(40) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `allergene`
--

INSERT INTO `allergene` (`allergeneID`, `nome`) VALUES
(1, 'Uova'),
(2, 'Latte'),
(3, 'Arachidi'),
(4, 'Soia'),
(5, 'Grano'),
(6, 'Pesci'),
(7, 'Crostacei'),
(8, 'Frutta a guscio');

-- --------------------------------------------------------

--
-- Struttura della tabella `base`
--

CREATE TABLE `base` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nome` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `base`
--

INSERT INTO `base` (`id`, `nome`) VALUES
(1, 'piada'),
(2, 'panino'),
(3, 'pizza');

-- --------------------------------------------------------

--
-- Struttura della tabella `base_ingrediente`
--

CREATE TABLE `base_ingrediente` (
  `baseID` bigint(20) UNSIGNED NOT NULL,
  `nome` char(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `base_ingrediente`
--

INSERT INTO `base_ingrediente` (`baseID`, `nome`) VALUES
(1, 'acqua'),
(1, 'farina'),
(2, 'acqua'),
(2, 'farina'),
(3, 'acqua'),
(3, 'farina');

-- --------------------------------------------------------

--
-- Struttura della tabella `farcitura`
--

CREATE TABLE `farcitura` (
  `piattoID` bigint(20) NOT NULL,
  `nome` char(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `farcitura`
--

INSERT INTO `farcitura` (`piattoID`, `nome`) VALUES
(1, 'prosciutto crudo'),
(2, 'prosciutto cotto'),
(3, 'mozzarella'),
(3, 'prosciutto cotto');

-- --------------------------------------------------------

--
-- Struttura della tabella `ingrediente`
--

CREATE TABLE `ingrediente` (
  `nome` char(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `ingrediente`
--

INSERT INTO `ingrediente` (`nome`) VALUES
('acqua'),
('farina'),
('latte'),
('maionese'),
('mozzarella'),
('patata'),
('pomodoro'),
('prosciutto cotto'),
('prosciutto crudo'),
('riso');

-- --------------------------------------------------------

--
-- Struttura della tabella `ingrediente_allergene`
--

CREATE TABLE `ingrediente_allergene` (
  `allergeneID` tinyint(50) NOT NULL,
  `nome` char(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `ingrediente_allergene`
--

INSERT INTO `ingrediente_allergene` (`allergeneID`, `nome`) VALUES
(1, 'maionese'),
(2, 'latte'),
(2, 'mozzarella'),
(2, 'prosciutto cotto'),
(5, 'farina');

-- --------------------------------------------------------

--
-- Struttura della tabella `ordine`
--

CREATE TABLE `ordine` (
  `id` bigint(20) NOT NULL,
  `compratoreID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `piattoID` bigint(20) NOT NULL,
  `ristoID` bigint(20) UNSIGNED NOT NULL,
  `quantita` int(20) NOT NULL,
  `tipologiaOrdine` set('EVASO','ACCETTATO','INVIATO','CARRELLO','CONSEGNATO') COLLATE utf8_unicode_ci NOT NULL,
  `orario` datetime DEFAULT NULL,
  `luogo` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fattorinoID` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `ordine`
--

INSERT INTO `ordine` (`id`, `compratoreID`, `piattoID`, `ristoID`, `quantita`, `tipologiaOrdine`, `orario`, `luogo`, `fattorinoID`) VALUES
(6, 'cassonaro@gmail.com', 2, 2, 1, 'EVASO', '2019-01-26 03:27:00', 'Aula Studio', 'fatt@fatt.com'),
(7, 'cassonaro@gmail.com', 1, 2, 3, 'ACCETTATO', '2019-01-26 03:27:00', 'Aula Magna', 'fatt@fatt.com'),
(8, 'cassonaro@gmail.com', 1, 2, 3, 'EVASO', '2019-01-26 03:27:00', 'Aula Studio', 'fatt@fatt.com'),
(9, 'cassonaro@gmail.com', 2, 2, 6, 'EVASO', '2019-01-26 03:27:00', 'Aula Magna', 'fatt@fatt.com'),
(10, 'cassonaro@gmail.com', 1, 1, 1, 'INVIATO', '2019-01-26 03:27:00', 'Aula Studio', NULL),
(10, 'cassonaro@gmail.com', 1, 2, 1, 'ACCETTATO', '2019-01-26 03:27:00', 'Aula Studio', 'fatt@fatt.com'),
(10, 'cassonaro@gmail.com', 2, 2, 3, 'ACCETTATO', '2019-01-26 03:27:00', 'Aula Studio', 'fatt@fatt.com'),
(11, 'evangelisti3@gmail.com', 1, 1, 7, 'CONSEGNATO', '2019-01-26 03:27:00', 'Aula Magna', NULL),
(11, 'evangelisti3@gmail.com', 1, 2, 3, 'ACCETTATO', '2019-01-26 03:27:00', 'Aula Magna', 'fatt@fatt.com'),
(11, 'evangelisti3@gmail.com', 2, 2, 2, 'ACCETTATO', '2019-01-26 03:27:00', 'Aula Magna', 'fatt@fatt.com'),
(11, 'evangelisti3@gmail.com', 3, 1, 3, 'CONSEGNATO', '2019-01-26 03:27:00', 'Aula Magna', NULL),
(11, 'evangelisti3@gmail.com', 3, 2, 1, 'ACCETTATO', '2019-01-26 03:27:00', 'Aula Magna', 'fatt@fatt.com'),
(12, 'maria.rosticceria@gmail.com', 1, 1, 2, 'INVIATO', '2019-01-26 03:27:00', 'Aula Studio', NULL),
(12, 'maria.rosticceria@gmail.com', 3, 1, 3, 'INVIATO', '2019-01-26 03:27:00', 'Aula Studio', NULL),
(13, 'cassonaro@gmail.com', 1, 2, 3, 'ACCETTATO', '2019-01-26 03:27:00', 'Aula Studio', 'fatt@fatt.com'),
(13, 'cassonaro@gmail.com', 2, 2, 2, 'ACCETTATO', '2019-01-26 03:27:00', 'Aula Studio', 'fatt@fatt.com'),
(13, 'cassonaro@gmail.com', 3, 2, 1, 'ACCETTATO', '2019-01-26 03:27:00', 'Aula Studio', 'fatt@fatt.com'),
(14, 'cassonaro@gmail.com', 1, 1, 1, 'INVIATO', '2019-01-26 03:27:00', 'Aula ristoro', NULL),
(14, 'cassonaro@gmail.com', 3, 1, 3, 'INVIATO', '2019-01-26 03:27:00', 'Aula ristoro', NULL),
(15, 'cassonaro@gmail.com', 1, 1, 3, 'INVIATO', '2019-01-26 03:27:00', 'Aula ristoro', NULL),
(15, 'cassonaro@gmail.com', 3, 1, 3, 'INVIATO', '2019-01-26 03:27:00', 'Aula ristoro', NULL),
(16, 'davide.evangelisti2@studio.unibo.it', 3, 1, 2, 'INVIATO', '2019-01-26 01:30:00', 'Aula ristoro', NULL),
(17, 'davide.evangelisti2@studio.unibo.it', 3, 1, 1, 'INVIATO', '2019-01-26 07:48:00', 'Aula Studio', NULL),
(18, 'davide.evangelisti2@studio.unibo.it', 1, 1, 9, 'CARRELLO', '2019-01-26 01:30:00', 'Aula ristoro', NULL),
(18, 'davide.evangelisti2@studio.unibo.it', 3, 1, 2, 'CARRELLO', '2019-01-26 01:30:00', 'Aula ristoro', NULL),
(19, 'evangelisti3@gmail.com', 1, 1, 3, 'INVIATO', '2019-01-29 14:58:00', 'Aula Magna', NULL),
(19, 'evangelisti3@gmail.com', 3, 1, 4, 'INVIATO', '2019-01-29 14:58:00', 'Aula Magna', NULL),
(20, 'maria.rosticceria@gmail.com', 1, 1, 1, 'CARRELLO', NULL, NULL, NULL),
(20, 'maria.rosticceria@gmail.com', 3, 2, 4, 'CARRELLO', NULL, NULL, NULL),
(21, 'cassonaro@gmail.com', 1, 2, 3, 'EVASO', '2019-01-29 13:50:00', 'Aula Magna', 'fatt@fatt.com'),
(21, 'cassonaro@gmail.com', 3, 2, 2, 'EVASO', '2019-01-29 13:50:00', 'Aula Magna', 'fatt@fatt.com'),
(22, 'evangelisti3@gmail.com', 3, 2, 2, 'CONSEGNATO', '2019-01-29 14:47:00', 'Aula Studio', 'fatt@fatt.com'),
(23, 'evangelisti3@gmail.com', 3, 1, 1, 'ACCETTATO', '2019-01-29 15:03:00', 'Aula ristoro', 'fatt@fatt.com'),
(24, 'fatt@fatt.com', 1, 1, 2, 'INVIATO', '2019-01-29 15:31:00', 'Aula Studio', NULL),
(24, 'fatt@fatt.com', 3, 1, 2, 'INVIATO', '2019-01-29 15:31:00', 'Aula Studio', NULL),
(26, 'evangelisti3@gmail.com', 1, 1, 11, 'CARRELLO', NULL, NULL, NULL),
(26, 'evangelisti3@gmail.com', 1, 2, 2, 'CARRELLO', NULL, NULL, NULL),
(26, 'evangelisti3@gmail.com', 3, 1, 4, 'CARRELLO', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struttura della tabella `piatto`
--

CREATE TABLE `piatto` (
  `piattoID` bigint(20) NOT NULL,
  `nomePiatto` char(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `piatto`
--

INSERT INTO `piatto` (`piattoID`, `nomePiatto`) VALUES
(1, 'piadina con crudo'),
(2, 'piadina con cotto'),
(3, 'piadina con cotto e mozzarella');

-- --------------------------------------------------------

--
-- Struttura della tabella `piatto_base`
--

CREATE TABLE `piatto_base` (
  `piattoID` bigint(20) NOT NULL,
  `baseID` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `piatto_base`
--

INSERT INTO `piatto_base` (`piattoID`, `baseID`) VALUES
(1, 1),
(2, 1),
(3, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `piatto_ristorante`
--

CREATE TABLE `piatto_ristorante` (
  `piattoID` bigint(20) NOT NULL,
  `ristoID` bigint(20) UNSIGNED NOT NULL,
  `costo` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `piatto_ristorante`
--

INSERT INTO `piatto_ristorante` (`piattoID`, `ristoID`, `costo`) VALUES
(1, 1, '3.00'),
(1, 2, '3.20'),
(2, 2, '2.50'),
(3, 1, '3.50'),
(3, 2, '3.20');

-- --------------------------------------------------------

--
-- Struttura della tabella `recensione`
--

CREATE TABLE `recensione` (
  `compratoreID` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ristoID` bigint(20) UNSIGNED NOT NULL,
  `stelle` tinyint(4) NOT NULL,
  `descrizione` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `recensione`
--

INSERT INTO `recensione` (`compratoreID`, `ristoID`, `stelle`, `descrizione`) VALUES
('davide.evangelisti2@studio.unibo.it', 1, 5, 'il tempo va passano le ore'),
('maria.rosticceria@gmail.com', 1, 1, 'Recensione seria.'),
('simone.magnani4@studio.unibo.it', 1, 5, 'The best place in Cesena!');

-- --------------------------------------------------------

--
-- Struttura della tabella `ristorante`
--

CREATE TABLE `ristorante` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nome` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `logopiccolo` tinytext COLLATE utf8_unicode_ci,
  `logogrande` tinytext COLLATE utf8_unicode_ci,
  `gestoreID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `descrizione` varchar(1500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tempoConsegnaMin` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `ristorante`
--

INSERT INTO `ristorante` (`id`, `nome`, `email`, `logopiccolo`, `logogrande`, `gestoreID`, `descrizione`, `tempoConsegnaMin`) VALUES
(1, 'Rosticceria Maria', 'lunedei.mariarosticceria@gmail.com', 'file.jpg', NULL, 'maria.rosticceria@gmail.com', 'descrizione', 20),
(2, 'Cassonaro', 'cassonaro@gmail.com', 'file.jpg', NULL, 'cassonaro@gmail.com', 'piccolo cassonaro', 5),
(13, 'Ris', 'email@gmail.com', 'file.jpg', NULL, 'cassonaro@gmail.com', 'descrizione', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `utente`
--

CREATE TABLE `utente` (
  `nome` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `cognome` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data_di_nascita` date NOT NULL,
  `tipologia` set('Admin','compratore','ristoratore','fattorino') COLLATE utf8_unicode_ci NOT NULL,
  `nickname` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `foto` tinytext COLLATE utf8_unicode_ci,
  `ristoID` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `utente`
--

INSERT INTO `utente` (`nome`, `cognome`, `email`, `password`, `data_di_nascita`, `tipologia`, `nickname`, `foto`, `ristoID`) VALUES
('Cassonaro', 'Cassone', 'cassonaro@gmail.com', '*098DB0F02F5DAF0FFEAB38911BA12A2E4611BEF7', '2019-01-01', 'ristoratore', 'cass', NULL, NULL),
('davide', 'evangelisti', 'davide.evangelisti2@studio.unibo.it', '*E46279BEA2E61E686DC78D078615543275EA36EC', '1995-06-10', 'Admin', 'kamina', 'img/kamina.jpeg', NULL),
('Davide', 'Evangelisti', 'evangelisti3@gmail.com', '*E46279BEA2E61E686DC78D078615543275EA36EC', '2019-01-08', 'compratore', 'kamina30', NULL, NULL),
('Fatto', 'Rino', 'fatt@fatt.com', '*90B29E0286B8DAC3063B4F1BB15E47B831D7E1AD', '2000-01-01', 'fattorino', 'fatt', NULL, 2),
('maria', 'la tipa della ro', 'maria.rosticceria@gmail.com', '*8061C323A725701555411A7E18421F077A840CD7', '1945-12-02', 'ristoratore', 'lamaria', NULL, NULL),
('mirko', 'fabbri', 'mirko.fabbri3@studio.unibo.it', '*87EE4B645873949F2031BCCB10D1B43C519FE716', '1997-01-19', 'Admin', 'wem', NULL, NULL),
('Rist', 'Oratore', 'rist@orato.it', '*74DBA7FB39AD84EACEE40A17058FCEA5B25F1545', '2001-02-01', 'ristoratore', 'rist', NULL, NULL),
('simone', 'magnani', 'simone.magnani4@studio.unibo.it', '*461371F1F9E3F985B7A3F6E8D24E8BAAD5295B45', '1997-12-18', 'Admin', 'magna', NULL, NULL),
('user', 'cognome2', 'user@gmail.com', '*DE931EF672523A79B61F82010B51DDA42FBB4240', '2003-02-05', 'compratore', 'utente', NULL, NULL);

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `allergene`
--
ALTER TABLE `allergene`
  ADD PRIMARY KEY (`allergeneID`);

--
-- Indici per le tabelle `base`
--
ALTER TABLE `base`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indici per le tabelle `base_ingrediente`
--
ALTER TABLE `base_ingrediente`
  ADD PRIMARY KEY (`baseID`,`nome`),
  ADD KEY `toingr2` (`nome`);

--
-- Indici per le tabelle `farcitura`
--
ALTER TABLE `farcitura`
  ADD PRIMARY KEY (`piattoID`,`nome`),
  ADD KEY `toingr3` (`nome`);

--
-- Indici per le tabelle `ingrediente`
--
ALTER TABLE `ingrediente`
  ADD PRIMARY KEY (`nome`);

--
-- Indici per le tabelle `ingrediente_allergene`
--
ALTER TABLE `ingrediente_allergene`
  ADD PRIMARY KEY (`allergeneID`,`nome`),
  ADD KEY `toingr` (`nome`);

--
-- Indici per le tabelle `ordine`
--
ALTER TABLE `ordine`
  ADD PRIMARY KEY (`id`,`compratoreID`,`piattoID`,`ristoID`),
  ADD KEY `compr` (`compratoreID`),
  ADD KEY `ordinepiatto` (`piattoID`),
  ADD KEY `ordinerist` (`ristoID`),
  ADD KEY `ordinetofatt` (`fattorinoID`);

--
-- Indici per le tabelle `piatto`
--
ALTER TABLE `piatto`
  ADD PRIMARY KEY (`piattoID`);

--
-- Indici per le tabelle `piatto_base`
--
ALTER TABLE `piatto_base`
  ADD PRIMARY KEY (`piattoID`,`baseID`),
  ADD KEY `tobase2` (`baseID`);

--
-- Indici per le tabelle `piatto_ristorante`
--
ALTER TABLE `piatto_ristorante`
  ADD PRIMARY KEY (`piattoID`,`ristoID`),
  ADD KEY `topiatt` (`piattoID`),
  ADD KEY `torist` (`ristoID`);

--
-- Indici per le tabelle `recensione`
--
ALTER TABLE `recensione`
  ADD PRIMARY KEY (`compratoreID`,`ristoID`),
  ADD KEY `recensioneto` (`ristoID`);

--
-- Indici per le tabelle `ristorante`
--
ALTER TABLE `ristorante`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `email index` (`email`),
  ADD KEY `togest` (`gestoreID`);

--
-- Indici per le tabelle `utente`
--
ALTER TABLE `utente`
  ADD PRIMARY KEY (`email`),
  ADD UNIQUE KEY `nick index` (`nickname`),
  ADD KEY `fatttorist` (`ristoID`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `allergene`
--
ALTER TABLE `allergene`
  MODIFY `allergeneID` tinyint(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT per la tabella `base`
--
ALTER TABLE `base`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT per la tabella `ordine`
--
ALTER TABLE `ordine`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT per la tabella `piatto`
--
ALTER TABLE `piatto`
  MODIFY `piattoID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT per la tabella `ristorante`
--
ALTER TABLE `ristorante`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `base_ingrediente`
--
ALTER TABLE `base_ingrediente`
  ADD CONSTRAINT `tobase` FOREIGN KEY (`baseID`) REFERENCES `base` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `toingr2` FOREIGN KEY (`nome`) REFERENCES `ingrediente` (`nome`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limiti per la tabella `farcitura`
--
ALTER TABLE `farcitura`
  ADD CONSTRAINT `toingr3` FOREIGN KEY (`nome`) REFERENCES `ingrediente` (`nome`),
  ADD CONSTRAINT `topiatto` FOREIGN KEY (`piattoID`) REFERENCES `piatto` (`piattoID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limiti per la tabella `ingrediente_allergene`
--
ALTER TABLE `ingrediente_allergene`
  ADD CONSTRAINT `toallerg` FOREIGN KEY (`allergeneID`) REFERENCES `allergene` (`allergeneID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `toingr` FOREIGN KEY (`nome`) REFERENCES `ingrediente` (`nome`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limiti per la tabella `ordine`
--
ALTER TABLE `ordine`
  ADD CONSTRAINT `compr` FOREIGN KEY (`compratoreID`) REFERENCES `utente` (`email`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ordinepiatto` FOREIGN KEY (`piattoID`) REFERENCES `piatto_ristorante` (`piattoID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ordinerist` FOREIGN KEY (`ristoID`) REFERENCES `ristorante` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ordinetofatt` FOREIGN KEY (`fattorinoID`) REFERENCES `utente` (`email`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Limiti per la tabella `piatto_base`
--
ALTER TABLE `piatto_base`
  ADD CONSTRAINT `tobase2` FOREIGN KEY (`baseID`) REFERENCES `base` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `topiatto2` FOREIGN KEY (`piattoID`) REFERENCES `piatto` (`piattoID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limiti per la tabella `piatto_ristorante`
--
ALTER TABLE `piatto_ristorante`
  ADD CONSTRAINT `topiatt` FOREIGN KEY (`piattoID`) REFERENCES `piatto` (`piattoID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `torist` FOREIGN KEY (`ristoID`) REFERENCES `ristorante` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limiti per la tabella `recensione`
--
ALTER TABLE `recensione`
  ADD CONSTRAINT `recensionefrom` FOREIGN KEY (`compratoreID`) REFERENCES `utente` (`email`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `recensioneto` FOREIGN KEY (`ristoID`) REFERENCES `ristorante` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limiti per la tabella `ristorante`
--
ALTER TABLE `ristorante`
  ADD CONSTRAINT `togest` FOREIGN KEY (`gestoreID`) REFERENCES `utente` (`email`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limiti per la tabella `utente`
--
ALTER TABLE `utente`
  ADD CONSTRAINT `fatttorist` FOREIGN KEY (`ristoID`) REFERENCES `ristorante` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
