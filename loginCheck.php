<?php
session_start();
include 'static.php';
function Login()
{
    if (empty($_POST['username'])) {
        return false;
    }

    if (empty($_POST['password'])) {
        return false;
    }

    $username = trim($_POST['username']);
    $password = trim($_POST['password']);

    if (!MyClass::checkLogin($username, $password)) {
        echo 'login fail';
        return ;
    }
    $_SESSION["nickname"] = $username;
    $_SESSION["tipologia"] = MyClass::getTipologiaFromNick($username);
    echo 'ok';
    return;
}

function Registration()
{
    $error = "";
    if (empty($_POST['name']))
    $error .= 'name error</br>';
    else
    $name = $_POST['name'];

    if (empty($_POST['surname']))
    $error .= 'surname errore</br>';
    else
    $surname = $_POST['surname'];

    if (empty($_POST['date']))
    $error .= 'data vuota</br>';
    else
    $date = $_POST['date'];

    if (empty($_POST['nickname']))
    $error .= 'nickname vuoto</br>';
    else
    $nickname = $_POST['nickname'];

    if (empty($_POST['mail']))
    $error .= 'mail vuota</br>';
    else
    $email = $_POST['mail'];

    if (empty(($_POST['tipologia'])))
    $tipologia = 'compratore';
    else
    $tipologia = $_POST['tipologia'];

    if (empty($_POST['password']) || empty($_POST['password2']))
    $error .= ' password errate </br>';
    else {
        $password = $_POST['password'];
        $password2 = $_POST['password2'];
        if ($password != $password2) {
            $error .= 'password diverse</br>';
        }

    }
    if (strlen($error) > 0) {
        echo $error;
        return;
    }

    $conn = MyClass::login();
    $sql = "SELECT * FROM utente WHERE nickname='$nickname' OR email='$email'";
    $result = $conn->query($sql);
    if ($result->num_rows == 0) {
        $sql = "INSERT INTO utente (nome, cognome, email, password, data_di_nascita, tipologia, nickname) VALUES ('$name', '$surname', '$email', PASSWORD('$password'), '$date', '$tipologia', '$nickname')";
        if ($conn->query($sql) === TRUE) {
            $conn->close();
            echo 'ok';
            return;
        }
        $conn->close();
        echo 'registrazione fallita';
        return;
    } else {
        /*todo nome utente o email già utilizzata*/
        $conn->close();
        echo 'errore utente già registrato';
        return;
    }
    $conn->close();
    echo 'fail';
    return;
}
if (isset($_POST["make"])) {
    if ($_POST["make"] == "registrazione") {
        Registration();
    } else if ($_POST["make"] == "login") {
        Login();
    }
}
?>
