function updateRist() {
  risto = $("#ristoranti").val();
  $.post("fattorino.php", {
      ristup: risto
    },
    function (data) {
      location.reload();
    });
}

$(document).ready(function () {
  var mymap = L.map('cesenamap').setView([44.1478, 12.2356], 15);

  L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
    maxZoom: 18,
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
      '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
      'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    id: 'mapbox.streets'
  }).addTo(mymap);

  var marker = L.marker([44.1478, 12.2356]).addTo(mymap);

  var way;
  var fattorino;

  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(showPosition);
  } else {
    x.innerHTML = "Geolocation is not supported by this browser.";
  }

  function showPosition(position) {
    console.log(position.coords.latitude);
    console.log(position.coords.longitude);

    way = L.polygon([
      [44.1478, 12.2356],
      [44.1478, 12.2356],
      [position.coords.latitude, position.coords.longitude],
      [position.coords.latitude, position.coords.longitude]
    ]).addTo(mymap);
    way.bindPopup("<b>Ecco come dovresti</br>viaggiare</b>");

    fattorino = L.marker([position.coords.latitude, position.coords.longitude]).addTo(mymap);
    fattorino.bindPopup("<b>Qui e' dove </br>ti trovi</b>").openPopup()

  }

  marker.bindPopup("<b>Questa e' la tua</br>destinazione</b>").openPopup();

  $.getJSON("fattorino.php?request=ristoranti", function (data) {
    console.log(data);
    for (var i = 0; i < data.length; i++) {
      local_id = data[i]["id"];
      local_nome = data[i]["nome"];
      $("#ristoranti").append("<option value='" + local_id + "'>" + local_id + " - " + local_nome + "</option>");
    }
  });

  $.getJSON("fattorino.php?request=ristoranti", function (data) {
    for (var i = 0; i < data.length; i++) {
      local_id = data[i]["id"];
      local_nome = data[i]["nome"];
      $("#ristoranti").append("<option value='" + local_id + "'>" + local_id + " - " + local_nome + "</option>");
    }
  });

  $.getJSON("fattorino.php?request=miorist", function (data) {
    
    for (var i = 0; i < data.length; i++) {
      local_id = data[i]["id"];
      local_nome = data[i]["nome"];
      $("#ristorante").append(local_id + " - " + local_nome);
    }
  });
});