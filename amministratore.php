<?php
session_start();
include 'static.php';
header('Content-Type: application/json');
if(isset($_GET["request"])) {

	$conn = MyClass::login();
	$rows_per_page = 3;

	switch ($_GET["request"]) {
		case "users":
      if (isset($_SESSION['nickname']) && MyClass::getTipologiaFromNick($_SESSION['nickname']) == "Admin") {

        $page = 0;
  			if(isset($_GET["page"])){
  				$page = $_GET["page"];

          $start_row = $page * $rows_per_page;

    			$stmt = $conn->prepare("SELECT * FROM utente LIMIT ?, ?");
    			$stmt->bind_param("ii", $start_row, $rows_per_page);
    			$stmt->execute();

    			$result = $stmt->get_result();

    			$output = array();
    			while($row = $result->fetch_assoc()) {
    				$output[] = $row;
    			}
    			$stmt->close();
    			print json_encode($output);
    			break;
  			} else if (isset($_GET["num_pages"])) {
          $stmt = $conn->prepare("SELECT COUNT(*) AS num_utenti FROM utente");
    			$stmt->execute();

    			$stmt->bind_result($num_users);
    			$stmt->fetch();

    			$num_pages_users = ceil($num_users / $rows_per_page);
    			$output = array("num_pages_users" => $num_pages_users);
    			print json_encode($output);
        }
      }
      break;

      case "restaurants":
        if (isset($_SESSION['nickname']) && MyClass::getTipologiaFromNick($_SESSION['nickname']) == "Admin") {
          $page = 0;
    			if(isset($_GET["page"])){
    				$page = $_GET["page"  ];

            $start_row = $page * $rows_per_page;

      			$stmt = $conn->prepare("SELECT * FROM ristorante LIMIT ?, ?");
      			$stmt->bind_param("ii", $start_row, $rows_per_page);
      			$stmt->execute();

      			$result = $stmt->get_result();

      			$output = array();
      			while($row = $result->fetch_assoc()) {
      				$output[] = $row;
      			}
      			$stmt->close();
      			print json_encode($output);
    			} else if (isset($_GET["num_pages"])) {
            $stmt = $conn->prepare("SELECT COUNT(*) AS num_ristoranti FROM ristorante");
      			$stmt->execute();

      			$stmt->bind_result($num_restaurants);
      			$stmt->fetch();

      			$num_pages_restaurants = ceil($num_restaurants / $rows_per_page);
      			$output = array("num_pages_restaurants" => $num_pages_restaurants);
      			print json_encode($output);
          }
        }
        break;

        case "allergens":
          if (isset($_SESSION['nickname']) && MyClass::getTipologiaFromNick($_SESSION['nickname']) == "Admin") {
            $page = 0;
            if(isset($_GET["page"])){
              $page = $_GET["page"];

              $start_row = $page * $rows_per_page;

              $stmt = $conn->prepare("SELECT * FROM allergene LIMIT ?, ?");
              $stmt->bind_param("ii", $start_row, $rows_per_page);
              $stmt->execute();

              $result = $stmt->get_result();

              $output = array();
              while($row = $result->fetch_assoc()) {
                $output[] = $row;
              }
              $stmt->close();
              print json_encode($output);
            } else if (isset($_GET["num_pages"])) {
              $stmt = $conn->prepare("SELECT COUNT(*) AS num_allergeni FROM allergene");
              $stmt->execute();

              $stmt->bind_result($num_allergens);
              $stmt->fetch();

              $num_pages_allergens = ceil($num_allergens / $rows_per_page);
              $output = array("num_pages_allergens" => $num_pages_allergens);
              print json_encode($output);
            }
          }
          break;
	}
}
?>
