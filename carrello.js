var availableTags = [
  "Aula Studio",
  "Aula Magna",
  "Entrata livello 2",
  "Entrata livello 3",
  "Aula ristoro",
  "Parcheggio interno",
  "Parcheggio esterno",
  "Piazzale bici livello 3",
  "Piazzale bici livello 2",
];

function rimuoviNelem(ordine, email, piatto, risto, num) {
  val = document.getElementById(num).value;
  $.get("ordine.php",{
    funct : 'removeNelem',
    ordine : ordine,
    email : email,
    piatto : piatto,
    risto : risto,
    num : val
  }, function(data) {
    console.log(data);
    if (data == 'ok') {
      location.reload();
    } else {
      alert("Errore");
    }
  });
}

function rimuovi(ordine, email, piatto, risto) {
  $.post("ordine.php", {
    ordine : ordine,
    email : email,
    piatto : piatto,
    risto : risto
  }, function(data) {
    if (data == 'ok') {
      location.reload();
    } else {
      alert("Errore");
    }
  });
}

function updateCarrello() {
  xmlhttp=preparexmlobj();
  xmlhttp.onreadystatechange=function() {
      if (this.readyState==4 && this.status==200) {
          document.getElementById("div_c").innerHTML=this.responseText;
          var myEle = document.getElementById("carrello_vuoto");
          if(myEle === null){
            document.getElementById("ui-widget").style.visibility = "visible";
            autocomplete(document.getElementById("myInput"), availableTags);
            $('#setTimeExample').timepicker();
            $('#setTimeButton').on('click', function (){
                $.get("ordine.php",{'funct' : "time"}, function(data){
                  var millis= new Date().getTime() + data*60*1000;
                  var dateToPass = new Date(millis);
                  $('#setTimeExample').timepicker('setTime', dateToPass);
                });
            });
          } else {

          }
      }
  }
  xmlhttp.open("GET","ordine.php?funct=showCart",true);
  xmlhttp.send();
  return ;
}

function inviaordine() {
  str=$('#setTimeExample')[0].value;
  index = availableTags.indexOf($('#myInput')[0].value);
  if (((str.length==6 && str.includes(":", 1) && (str.includes("am", 4) || str.includes("pm", 4))
            && (!isNaN(str.slice(0,1)) && (str.slice(0,1)<=12)) && (!isNaN(str.slice(2,4)) && (str.slice(2,4)<60)))
    || (str.length==7 && str.includes(":", 2) && (str.includes("am", 5) || str.includes("pm", 5))
            && (!isNaN(str.slice(0,2)) && (str.slice(0,2)<=12)) && (!isNaN(str.slice(3,5)) && (str.slice(3,5)<60)))) && (index > -1)) {
        prepareAJAX("div_c");
        xmlhttp.open("GET","ordine.php?funct=sendCart&time="+$('#setTimeExample')[0].value+"&luogo="+$('#myInput')[0].value,true);
        xmlhttp.send();
        location.reload(true);
  }
    return ;
}

function autocomplete(inp, arr) {

  var currentFocus;

  inp.addEventListener("input", function(e) {
      var a, b, i, val = this.value;
      closeAllLists();
      if (!val) { return false;}
      currentFocus = -1;
      a = document.createElement("DIV");
      a.setAttribute("id", this.id + "autocomplete-list");
      a.setAttribute("class", "autocomplete-items");
      this.parentNode.appendChild(a);
      for (i = 0; i < arr.length; i++) {
        if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
          b = document.createElement("DIV");
          b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
          b.innerHTML += arr[i].substr(val.length);
          b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
          b.addEventListener("click", function(e) {
              inp.value = this.getElementsByTagName("input")[0].value;
              closeAllLists();
          });
          a.appendChild(b);
        }
      }
  });

  inp.addEventListener("keydown", function(e) {
      var x = document.getElementById(this.id + "autocomplete-list");
      if (x) x = x.getElementsByTagName("div");
      if (e.keyCode == 40) {
        currentFocus++;
        addActive(x);
      } else if (e.keyCode == 38) {
        currentFocus--;
        addActive(x);
      } else if (e.keyCode == 13) {
        e.preventDefault();
        if (currentFocus > -1) {
          if (x) x[currentFocus].click();
        }
      }
  });

  function addActive(x) {
    if (!x) return false;
    removeActive(x);
    if (currentFocus >= x.length) currentFocus = 0;
    if (currentFocus < 0) currentFocus = (x.length - 1);
    x[currentFocus].classList.add("autocomplete-active");
  }

  function removeActive(x) {
    for (var i = 0; i < x.length; i++) {
      x[i].classList.remove("autocomplete-active");
    }
  }

  function closeAllLists(elmnt) {
    var x = document.getElementsByClassName("autocomplete-items");
    for (var i = 0; i < x.length; i++) {
      if (elmnt != x[i] && elmnt != inp) {
        x[i].parentNode.removeChild(x[i]);
      }
    }
  }

  document.addEventListener("click", function (e) {
      closeAllLists(e.target);
  });
}

$(document).ready(function () {
  updateCarrello();
});
