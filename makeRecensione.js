var star = -1;

function setStar(sno) {

    star = sno;
    for (var i = 1; i <= 5; i++) {
        var cur = document.getElementById("star" + i)
        cur.className = "fa fa-rocket"
    }

    for (var i = 1; i <= sno; i++) {
        var cur = document.getElementById("star" + i)
        if (cur.className == "fa fa-rocket") {
            cur.className = "fa fa-rocket text-warning"
        }
    }

}

function myupload() {
    event.preventDefault();
    var descrizione = "";
    var error = "";
    if (star == -1) {
        error += "valutazione non inserita";
    }
    if (error.length > 0) {
        return;
    }
    descrizione = $("#comment").val();
    $.post("recensione.php", {
        upload : restID,
        star : star,
        desc : descrizione
    },
        function (data) {
            myload("ristorante.html?restID="+restID);
            if (data === 'ok') {
                alert("Recensione caricata con successo");
            } else if (data === 'update') {
                alert("Recensione aggiornata con successo");
            } else if (data === 'errorupdate') {
                alert("update della recensione fallita");
            } else {
                alert("Caricamento fallito.");
            }
        }
    );
}
