<?php
session_start();
include 'static.php';
header('Content-Type: application/json');
if (isset($_GET["request"])) {

	$conn = MyClass::login();
	$rows_per_page = 3;

	switch ($_GET["request"]) {
		case "owned":
			if (isset($_SESSION['nickname']) && MyClass::getTipologiaFromNick($_SESSION['nickname']) == "ristoratore") {
				$page = 0;
				if (isset($_GET["page"])) {
					$page = $_GET["page"];

					$start_row = $page * $rows_per_page;
					$email = MyClass::getEmailFromNick($_SESSION['nickname']);

					$stmt = $conn->prepare("SELECT id, logopiccolo, nome FROM ristorante WHERE gestoreID = ? LIMIT ?, ?");
					$stmt->bind_param("sii", $email, $start_row, $rows_per_page);
					$stmt->execute();

					$result = $stmt->get_result();

					$output = array();
					while ($row = $result->fetch_assoc()) {
						$output[] = $row;
					}
					$stmt->close();
					print json_encode($output);
					break;
				} else if (isset($_GET["num_pages"])) {
					if (isset($_SESSION['nickname']) && MyClass::getTipologiaFromNick($_SESSION['nickname']) == "ristoratore") {
						$stmt = $conn->prepare("SELECT COUNT(*) AS num_risto FROM ristorante WHERE gestoreID = ?");
						$stmt->bind_param("s", $email);
						$stmt->execute();
						$result = $stmt->get_resutl();
						$stmt->bind_result($num_owned);
						$stmt->fetch();

						$num_pages_owned = ceil($num_owned / $rows_per_page);
						$output = array("num_pages_owned" => $num_pages_owned);
						print json_encode($output);
					}
				}
			}
			break;

		case "list":
			if (isset($_SESSION['nickname']) && MyClass::getTipologiaFromNick($_SESSION['nickname']) == "ristoratore") {
				$page = 0;
				if (isset($_GET["page"])) {
					$page = $_GET["page"];

					$start_row = $page * $rows_per_page;
					$email = MyClass::getEmailFromNick($_SESSION['nickname']);

					$stmt = $conn->prepare("SELECT nomePiatto, costo, pr.piattoID, pr.ristoID FROM piatto AS p JOIN piatto_ristorante AS pr JOIN ristorante AS r ON p.piattoID = pr.piattoID AND pr.ristoID = r.id WHERE r.gestoreID = ? LIMIT ?, ?");
					$stmt->bind_param("sii", $email, $start_row, $rows_per_page);
					$stmt->execute();

					$result = $stmt->get_result();

					$output = array();
					while ($row = $result->fetch_assoc()) {
						$output[] = $row;
					}
					$stmt->close();
					print json_encode($output);
				} else if (isset($_GET["num_pages"])) {
					if (isset($_SESSION['nickname']) && MyClass::getTipologiaFromNick($_SESSION['nickname']) == "ristoratore") {
						$stmt = $conn->prepare("SELECT COUNT(*) AS num_list FROM piatto_ristorante WHERE gestoreID = ?");
						$stmt->bind_param("s", $email);
						$stmt->execute();
						$result = $stmt->get_resutl();
						$stmt->bind_result($num_list);
						$stmt->fetch();

						$num_pages_list = ceil($num_list / $rows_per_page);
						$output = array("num_pages_list" => $num_pages_list);
						print json_encode($output);
					}
				}
			}
			break;

		case "course":
			if (isset($_SESSION['nickname']) && (MyClass::getTipologiaFromNick($_SESSION['nickname']) === 'ristoratore' || MyClass::getTipologiaFromNick($_SESSION['nickname']) == 'Admin')) {
				$stmt = $conn->prepare("SELECT piattoID, nomePiatto FROM piatto");
				$stmt->execute();

				$result = $stmt->get_result();

				$output = array();
				while ($row = $result->fetch_assoc()) {
					$output[] = $row;
				}
				$stmt->close();
				print json_encode($output);
			}
			break;

		case "risto":
			if (isset($_SESSION['nickname']) && MyClass::getTipologiaFromNick($_SESSION['nickname']) == "ristoratore") {

				$email = MyClass::getEmailFromNick($_SESSION['nickname']);

				$stmt = $conn->prepare("SELECT id FROM ristorante WHERE gestoreID = ?");
				$stmt->bind_param("s", $email);
				$stmt->execute();

				$result = $stmt->get_result();

				$output = array();
				while ($row = $result->fetch_assoc()) {
					$output[] = $row;
				}
				$stmt->close();
				print json_encode($output);
			}
			break;

	case "allallergeni":
			if (isset($_SESSION['nickname']) && (MyClass::getTipologiaFromNick($_SESSION['nickname']) === 'ristoratore' || MyClass::getTipologiaFromNick($_SESSION['nickname']) == 'Admin')) {
				$sql = "SELECT `allergene`.allergeneID, `allergene`.nome FROM `allergene`";
				$result = MyClass::queryIt($sql,false);
				while ($row = $result->fetch_assoc()) {
					$output[] = $row;
				}
				print json_encode($output);
			}
			break;
	case "ingredienti":
			if (isset($_SESSION['nickname']) && (MyClass::getTipologiaFromNick($_SESSION['nickname']) === 'ristoratore' || MyClass::getTipologiaFromNick($_SESSION['nickname']) == 'Admin')) {
				$sql = "SELECT `ingrediente`.nome FROM `ingrediente`";
				$result = MyClass::queryIt($sql,false);
				while ($row = $result->fetch_assoc()) {
					$output[] = $row;
				}
				print json_encode($output);
			}
			break;
		case "basi":
			if (isset($_SESSION['nickname']) && (MyClass::getTipologiaFromNick($_SESSION['nickname']) === 'ristoratore' || MyClass::getTipologiaFromNick($_SESSION['nickname']) == 'Admin')) {
				$sql = "SELECT `base`.id, `base`.nome FROM `base`";
				$result = MyClass::queryIt($sql,false);
				while ($row = $result->fetch_assoc()) {
					$output[] = $row;
				}
				print json_encode($output);
			}
			break;
		case "ristoranti":
			if (isset($_SESSION['nickname']) && MyClass::getTipologiaFromNick($_SESSION['nickname']) === 'ristoratore') {
				$nick = $_SESSION['nickname'];
				$sql = "SELECT `ristorante`.id, `ristorante`.nome FROM `utente` JOIN `ristorante` ON `ristorante`.gestoreID = `utente`.email
				WHERE utente.nickname ='$nick' ";
				$result = MyClass::queryIt($sql,false);
				while ($row = $result->fetch_assoc()) {
					$output[] = $row;
				}
				print json_encode($output);
			} else if (isset($_SESSION['nickname']) && MyClass::getTipologiaFromNick($_SESSION['nickname']) === 'Admin') {
				$sql = "SELECT `ristorante`.id, `ristorante`.nome FROM `ristorante`";
				$result = MyClass::queryIt($sql,false);
				while ($row = $result->fetch_assoc()) {
					$output[] = $row;
				}
				print json_encode($output);
			}
			break;
	}
	$conn->close();
} else if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_SESSION['nickname']) &&
	MyClass::getTipologiaFromNick($_SESSION['nickname']) == "ristoratore") {
	$conn = MyClass::login();
	if (isset($_POST['piatto']) && isset($_POST['prezzo']) && isset($_POST['risto'])) {
		$stmt = $conn->prepare("SELECT piattoID FROM piatto WHERE nomePiatto = ?");
		$stmt->bind_param("s", $_POST["piatto"]);
		$stmt->execute();
		$result = $stmt->get_resutl();
		$stmt->bind_result($id);
		$stmt->fetch();
		$stmt = $conn->prepare("INSERT INTO piatto_ristorante (piattoID, ristoID, costo) VALUES (?, ?, ?)");
		$stmt->bind_param("iid", $id, $_POST['risto'], $_POST['prezzo']);
		$stmt->execute();
		$stmt->close();
	} else if (isset($_POST['nome'])) {
		$stmt = $conn->prepare("SELECT nomePiatto FROM piatto");
		$stmt->execute();
		$result = $stmt->get_result();

		$output = array();
		while ($row = $result->fetch_assoc()) {
			$output[] = $row;
		}
		$insert = true;
		for ($i = 0; $i < count($output); $i++) {
			if ($output[$i]['nomePiatto'] == $_POST['nome']) {
				$insert = false;
			}
		}
		if ($insert) {
			$stmt = $conn->prepare("INSERT INTO piatto (nomePiatto) VALUES (?)");
			$stmt->bind_param("s", $_POST['nome']);
			$stmt->execute();
			$stmt->close();
		}
	}
}
?>
