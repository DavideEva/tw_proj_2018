function manage_pages(current_page, num_pages, id){

	$("nav#" + id + " ul li").removeClass("disabled");
	$("nav#" + id + " ul li").removeClass("active");

	if(current_page ==0){
		$("nav#" + id + " ul li:first-child").addClass("disabled");
	}

	if(current_page == num_pages - 1){
		$("nav#" + id + " ul li:last-child").addClass("disabled");
	}

	$("nav#" + id + " ul li:nth-child(" + (current_page + 2) + ")").addClass("active");
}

function getFromDB(id, page) {
    $.getJSON("allergeni.php?request=" + id + "&page=" + page, function (data) {
        var html_code = "";
        for (var i = 0; i < data.length; i++) {
            if (id === "allergens") {
                html_code += '<tr><td>' + data[i]["allergeneID"] + '</td><td>' + data[i]["nome"];
            }
        }

        $("table#allergent" + " tbody").html(html_code);
    });
}

function setUp(id, page) {
    $.getJSON("allergeni.php?request=" + id + "&num_pages", function(data) {
      var num_pages = data["num_pages_" + id ];

      html_code = '<li class="page-item disabled"><a class="page-link" href="#">Prev</a></li>';
      for(let i = 0; i < num_pages; i++){
        html_code += '<li class="page-item"><a class="page-link" href="#">'+ (i+1) + '</a></li>';
      }
      html_code += '<li class="page-item"><a class="page-link" href="#">Next</a></li>';

      $("nav#" + id + " ul").html(html_code);

      manage_pages(page, num_pages, id);

      $("nav#"+ id + " ul li").click(function(){
        //controllo se ha classe active o disabled
        if(!$(this).hasClass("disabled") && !$(this).hasClass("active")){
          var contenuto = $(this).find("a").text();
          switch(contenuto) {
            case "Prev":
              page -= 1;
              break;
            case "Next":
              page += 1;
              break;
            default:
              page = contenuto - 1;
          }

          getFromDB(id, page);
          manage_pages(page, num_pages, id);
        }

      });

    });
  }

$(function () {
    var page = 0;
    id = "allergens";

    getFromDB(id, page);
    setUp(id, page);
});
