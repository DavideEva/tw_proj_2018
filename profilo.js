var data = null;
var type = "";
function upload(what, from) {
    val = $(from).val();
    $.post("profiloPost.php", {
      update : what,
      value : val,
      data : data
    },
    function (data) {
        if (data == 'ok') {
          alert("Valore aggiornato con successo!");
        } else {
          alert("Valore NON aggiornato!" + data);
        }
        window.location.href = 'profilo.html';
      }
    );
}

function uploadFoto(what, fromID) {
  var files;
  files = document.getElementById(fromID).files;
  for (var i = 0, f; f = files[i]; i++) {

      if (f.type.indexOf("image/") == 0) {
        type = "." + f.type.replace("image/", "");
    } else {
        type = '.jpeg';
    }

      var reader = new FileReader();
      reader.onload = (function(theFile) {
          return function(e) {
              // Render thumbnail.
              data = e.target.result;
              $.post("profiloPost.php", {
                update : what,
                value : type,
                data : data
              },
              function (data) {
                  if (data == 'ok') {
                    alert("Valore aggiornato con successo!");
                  } else {
                    alert("Valore NON aggiornato!" + data);
                  }
                  window.location.href = 'profilo.html';
                }
              );
          };
      })(f);
      reader.readAsDataURL(f);
  }

}

$(document).ready(function() {
  $.getJSON("profilo.php?request=profile", function(data) {
    $("#nome").text(data[0]);
    $("#cognome").text(data[1]);
    $("#email").text(data[2]);
    $("#nickname").text(data[4]);
    if (data[5] == null) {
      foto = 'img/file.jpg';
    } else {
      foto = data[5];
    }
    console.log(foto);
    $("#foto").attr('src', foto);
  });
});
