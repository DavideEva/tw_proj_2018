var restID;
function getName() {
    xmlhttp=preparexmlobj();
    xmlhttp.onreadystatechange=function() {
        if (this.readyState==4 && this.status==200) {
            document.getElementById("h1").innerHTML=this.responseText;
        }
    }
    str=window.location.href;
    id=str.substr(str.search('restID=')+'restID='.length);
    restID = id;
    xmlhttp.open("GET","ristorante.php?restID="+id+"&funct=1",true);
    xmlhttp.send();
    return ;
}

function calcolaPrezzo(idname, costo){
  num = new Number($( "#dialog_" + idname + " input#quantita" ).val() * costo);
  $('span#prezzo_' + idname).html("costo totale: " + num.toFixed(2));
}

function caricaCarrello(idname, costo, idrest, idPiatto){
  xmlhttp=preparexmlobj();
  xmlhttp.open("GET","ordine.php?quantita="+$( "#dialog_" + idname + " input#quantita" ).val()
                              +"&restID="+ idrest
                              +"&piattoID=" + idPiatto
                              +"&funct=add",true);
  xmlhttp.send();
}

function updatePiatti() {
  xmlhttp=preparexmlobj();
  xmlhttp.onreadystatechange=function() {
      if (this.readyState==4 && this.status==200) {
          document.getElementById("div").innerHTML=this.responseText;
      }
  }
  str=window.location.href;
  id=str.substr(str.search('restID=')+'restID='.length);
  xmlhttp.open("GET","ristorante.php?restID="+id+"&funct=2",true);
  xmlhttp.send();
  return ;
}

function checkProfilo() {
    $.get("ristorante.php?funct=3",
        function (data) {
            if (data === 'ok') {
            } else if (data === 'fail') {
                window.location.href = "login.html";
            } else {
                window.location.href = "home.html";
            }
        }
    );
}

checkProfilo();
$(function () {
    getName();
    updatePiatti();
    $("#doRecensione").load("makeRecensione.html");
    $("#recensioni").load("recensione.html");
});
