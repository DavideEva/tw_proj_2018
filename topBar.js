var actual = "home.html";
var restID = 0;

function myFunction() {
    var x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
        x.className += " responsive";
    } else {
        x.className = "topnav";
    }
}

function logOut() {
    p = $.post("logout.php", {
        logout: "true"
    });
    p.done(function (param) {
        if (param == 'ok') {
            window.location.replace("home.html");
        }
    });
}

/*Load .html file and put head in headSelector and body in bodySelector */
function myload(page = actual, reload = false, where = "#body") {

    window.location.href = page;
    return;
    if (page.indexOf("?") >= 0) {
        name = page.substring(0, page.indexOf("?"));
        $(where).load(name, function (e) {
            event.preventDefault();
            window.history.replaceState(null, null, page.substring(page.indexOf('?')));
        });

        p = name;
    } else {
        $(where).load(page);
        p = page;
    }

    //close topnav if open
    var x = document.getElementById("myTopnav");
    x.className = "topnav";



    if (reload === true) {
        reloadTopBar();
    }

    return;
}


function preparexmlobj() {
    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else { // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    return xmlhttp;
}

function controlNotify(item) {
    $.get("ordine.php", {
        funct: 'howManyNotify'
    }, function (data) {
        if (data > 0) {
            $(item).show();
        } else {
            $(item).hide();
        }
    });
}

function reloadTopBar() {
    $.post("topBar.php", {
            init: "init"
        },
        function (data) {
            $("#myTopnav").html(data);
        });
}


$(function () {
    actual = window.location.href;
    $.post("topBar.php", {
        setPage: actual,
    }, function () {

        reloadTopBar();
        $("nav#myTopnav.topnav a:not('.icon')").each(function () {
            // element == this
            if (this.id === actual) {
                this.className = "active";
                $("#myTopnav a:not('.icon')").each(function () {
                    if (this.id !== actual) {
                        this.className = "inActive";
                    }
                });
            }
        });
        setInterval(function () {
            controlNotify("#notifi");
        }, 1000);
    });
});
