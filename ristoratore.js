function manage_pages(current_page, num_pages, id){

	$("nav#" + id + " ul li").removeClass("disabled");
	$("nav#" + id + " ul li").removeClass("active");

	if(current_page ==0){
		$("nav#" + id + " ul li:first-child").addClass("disabled");
	}

	if(current_page == num_pages - 1){
		$("nav#" + id + " ul li:last-child").addClass("disabled");
	}

	$("nav#" + id + " ul li:nth-child(" + (current_page + 2) + ")").addClass("active");
}

function getFromDB(id, page) {
  $.getJSON("ristoratore.php?request=" + id + "&page=" + page, function(data) {
		var html_code = "";
		for(var i = 0; i < data.length; i++){
      if (id === "owned") {
        html_code += '<tr><td class="id">'+data[i]["id"]+'</td><td class="logopiccolo"><img src="img/'+data[i]["logopiccolo"]+'" alt="logo“ width="42" height="42"></td><td class="nome">'+data[i]["nome"]+'</td><td class="bottone"><input type="button" value="elimina" id="button-risto" onclick="eliminaRisto(' + data[i]["id"] +');" class="' + data[i]["id"] + '"></td></tr>';
      } else if (id === "list") {
        html_code += '<tr><td class="nomePiatto">'+data[i]["nomePiatto"]+'</td><td class="costo">'+data[i]["costo"]+'</td><td class="bottone"><input type="button" value="elimina" id="button-piatto" onclick="eliminaPiatto(' + data[i]["piattoID"] + ', ' + data[i]["ristoID"] + ')"></td></tr>';
      }
		}

		$("table#" + id + " tbody").html(html_code);
	});
}

function setUp(id, page) {
  $.getJSON("ristoratore.php?request=" + id + "&num_pages", function(data) {
    var num_pages = data["num_pages_" + id ];

    html_code = '<li class="page-item disabled"><a class="page-link" href="#">Prev</a></li>';
    for(let i = 0; i < num_pages; i++){
      html_code += '<li class="page-item"><a class="page-link" href="#">'+ (i+1) + '</a></li>';
    }
    html_code += '<li class="page-item"><a class="page-link" href="#">Next</a></li>';

    $("nav#" + id + " ul").html(html_code);

    manage_pages(page, num_pages, id);

    $("nav#"+ id + " ul li").click(function(){
      //controllo se ha classe active o disabled
      if(!$(this).hasClass("disabled") && !$(this).hasClass("active")){
        var contenuto = $(this).find("a").text();
        switch(contenuto) {
          case "Prev":
            page -= 1;
            break;
          case "Next":
            page += 1;
            break;
          default:
            page = contenuto - 1;
        }

        getFromDB(id, page);
        manage_pages(page, num_pages, id);
      }

    });

  });
}

function eliminaRisto(id){
  $.post("eliminazione.php",{
    state : "risto",
    id : id
  }, function(data){
    if (data == "ok"){
      alert("eliminazione terminata con successo!");
      location.reload();
    } else {
      alert("eliminazione non terminata");
    }
  });
}

function eliminaPiatto(idp, idr){

  $.post("eliminazione.php",{
    state : "piatto-risto",
    idp : idp ,
    idr : idr
  }, function(data){
    if (data == "ok"){
      alert("eliminazione terminata con successo!");
      location.reload();
    } else {
      alert("eliminazione non terminata");
    }
  });
}

$(document).ready(function(){
	var page = 0;
  var id = "owned";

  getFromDB(id, page);
  setUp(id, page);


  id = "list";

  getFromDB(id, page);
  setUp(id, page);


  $.getJSON("ristoratore.php?request=course", function(data) {
    var html_code = "";
    for(var i = 0; i < data.length; i++){
        html_code += '<option class="nomePiatto">'+data[i]["nomePiatto"]+'</option>';
    }
    $("form select#options-piatto").html(html_code);
  });

  $.getJSON("ristoratore.php?request=risto", function(data) {
    var html_code = "";
    for(var i = 0; i < data.length; i++){
        html_code += '<option class="id risto">'+data[i]["id"]+'</option>';
    }
    $("form select#options-risto").html(html_code);
  });

  $("#aggProd").load("aggiuntaProdotti.html");
});
