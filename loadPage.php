<?php
include 'static.php';
function check(String $hint = "") {
    $conn = MyClass::login();
    if ($hint == '') {
        $sql = "SELECT id,nome,descrizione,logopiccolo FROM ristorante";
    } else {
        $sql = "SELECT id,nome,descrizione,logopiccolo FROM ristorante where nome LIKE '%$hint%'";
    }
    $result = $conn->query($sql);

    foreach ($result as $value) {
        $logoPiccolo = 'img/'.$value["logopiccolo"];
        MyClass::createRestButton($value["nome"], $value["descrizione"], $logoPiccolo, $value["logopiccolo"], "ristorante.html?restID=" . $value["id"]);
    }
    return ;
}

function showBase(String $hint = "")
{
    $conn = MyClass::login();
    if ($hint == '') {
        $sql = "SELECT DISTINCT ristorante.nome, ristorante.descrizione, ristorante.id, ristorante.logopiccolo FROM ristorante
        JOIN piatto_ristorante ON piatto_ristorante.ristoID= ristorante.id
        JOIN piatto_base ON piatto_ristorante.piattoID= piatto_base.piattoID
        JOIN base ON piatto_base.baseID= base.id";
    } else {
        $sql = "SELECT DISTINCT ristorante.nome, ristorante.descrizione, ristorante.id, ristorante.logopiccolo FROM ristorante
        JOIN piatto_ristorante ON piatto_ristorante.ristoID= ristorante.id
        JOIN piatto_base ON piatto_ristorante.piattoID= piatto_base.piattoID
        JOIN base ON piatto_base.baseID= base.id
        WHERE base.nome LIKE '%$hint%'";
    }
    $result = $conn->query($sql);

    foreach ($result as $value) {
        $logoPiccolo = 'img/'.$value["logopiccolo"];
        MyClass::createRestButton($value["nome"], $value["descrizione"], $logoPiccolo, $logoPiccolo, "ristorante.html?restID=" . $value["id"]);
    }
    return ;
}

function loadBase(String $var = "")
{
    $conn = MyClass::login();
    $sql = "SELECT DISTINCT base.nome FROM ristorante
        JOIN piatto_ristorante ON piatto_ristorante.ristoID= ristorante.id
        JOIN piatto_base ON piatto_ristorante.piattoID= piatto_base.piattoID
        JOIN base ON piatto_base.baseID= base.id";
    $result = $conn->query($sql);
    foreach ($result as $value) {
        echo '  <div class="form-check disabled">
                    <label class="form-check-label">
                        <input type="checkbox" class="form-check-input" value="'. $value['nome'] .'" name="base" onchange="showBaseChecked()">'. $value['nome'] .'
                    </label>
                </div>';
    }
    return ;
}



if (isset($_GET['hint']))
    check($_GET['hint']);
else if (isset($_GET['base']))
    showBase($_GET['base']);
else if (isset($_GET['loadbase']))
    loadBase($_GET['loadbase']);

?>
