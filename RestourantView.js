function showResult(str) {
    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    } else {  // code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function() {
        if (this.readyState==4 && this.status==200) {
            document.getElementById("list").innerHTML=this.responseText;
        }
    }
    xmlhttp.open("GET","loadPage.php?hint="+str,true);
    xmlhttp.send();
}

function showBase(base) {
    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    } else {  // code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function() {
        if (this.readyState==4 && this.status==200) {
            document.getElementById("list").innerHTML=this.responseText;
        }
    }

    xmlhttp.open("GET","loadPage.php"+base,true);
    xmlhttp.send();
}

function loadBase(str = "") {
    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
    } else {  // code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange=function() {
        if (this.readyState==4 && this.status==200) {
            document.getElementById("formid").innerHTML=this.responseText;
        }
    }
    xmlhttp.open("GET","loadPage.php?loadbase=load" + str,true);
    xmlhttp.send();
}

function showBaseChecked() {

    var favorite = ["?"];
    $.each($(".form-check-input"), function(){
        console.log($(this).val());
        if (this.checked)
            favorite.push($(this).val());
    });

    v = favorite.join("&base=");
    v = v.replace("?&", "?");
    showBase(v);
}

function reloadStylesheets() {
    var queryString = 'css?reload=' + new Date().getTime();
    $('link[rel="stylesheet"]').each(function () {
        this.href = this.href.replace("/\?.*|$/", queryString);
    });
}

$(function () {
    showResult("");
    loadBase("");
    reloadStylesheets();
});
