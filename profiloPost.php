<?php
session_start();
include 'static.php';

if (isset($_SESSION['nickname']) && isset($_POST['update'])) {
    if ($_POST['update'] == 'password') {
        $pass = $_POST['value'];
        $email = MyClass::getEmailFromNick($_SESSION['nickname']);
        $sql = "UPDATE utente SET `utente`.password=PASSWORD('$pass') WHERE `utente`.email='$email'";
        if (MyClass::updateIt($sql, false)) {
            echo 'ok';
        } else {
            echo 'operazione non riuscita';
        }
    } else if ($_POST['update'] == 'nome') {
        $nome = $_POST['value'];
        $email = MyClass::getEmailFromNick($_SESSION['nickname']);
        $sql = "UPDATE utente SET `utente`.nome='$nome' WHERE `utente`.email='$email'";
        if (MyClass::updateIt($sql, false)) {
            echo 'ok';
        } else {
            echo 'operazione non riuscita';
        }
    } else if ($_POST['update'] == 'cognome') {
        $cognome = $_POST['value'];
        $email = MyClass::getEmailFromNick($_SESSION['nickname']);
        $sql = "UPDATE utente SET `utente`.cognome='$cognome' WHERE `utente`.email='$email'";
        if (MyClass::updateIt($sql, false)) {
            echo 'ok';
        } else {
            echo 'operazione non riuscita';
        }
    } else if ($_POST['update'] == 'nickname') {
        $nickname = $_POST['value'];
        $email = MyClass::getEmailFromNick($_SESSION['nickname']);
        $sql = "UPDATE utente SET `utente`.nickname='$nickname' WHERE `utente`.email='$email'";
        if (MyClass::updateIt($sql, false)) {
            echo 'ok';
        } else {
            echo 'operazione non riuscita';
        }
    } else if ($_POST['update'] == 'email') {
        $nemail = $_POST['value'];
        $email = MyClass::getEmailFromNick($_SESSION['nickname']);
        $sql = "UPDATE utente SET `utente`.email='$nemail' WHERE `utente`.email='$email'";
        if (MyClass::updateIt($sql, false)) {
            echo 'ok';
        } else {
            echo 'operazione non riuscita';
        }
    } else if ($_POST['update'] == 'foto') {
        $data = substr($_POST['data'], strpos($_POST['data'], ",") + 1);
    // decode it
        $decodedData = base64_decode($data);
        foreach (glob('img/'.$_SESSION['nickname'].'*') as $value) {
            unlink($value);
        }
        $fileName = "img/" . $_SESSION['nickname'] . $_POST['value'];
        $fp = fopen($fileName, 'wb');
        fwrite($fp, $decodedData);
        fclose($fp);
        $email = MyClass::getEmailFromNick($_SESSION['nickname']);
        $sql = "UPDATE utente SET `utente`.foto='$fileName' WHERE `utente`.email='$email'";
        if (MyClass::updateIt($sql, false)) {
            echo 'ok';
        } else {
            echo 'operazione non riuscita';
        }
    }
}

?>
